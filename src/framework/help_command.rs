use crate::Context;
use crate::core::consts::colors;
use super::command::{Command, CommandOrAlias, Module};
use std::collections::HashMap;
use std::{fmt, fmt::{Debug, Formatter}};
use std::error::Error as StdError;
use std::sync::Arc;
use super::args::Args;
use twilight_model::channel::Message;
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder};

#[derive(Clone)]
pub struct Help(pub HashMap<String, Arc<Module>>, pub Arc<HelpOptions>);

impl Debug for Help {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.debug_struct("Help")
            .field("options", &self.1)
            .finish()
    }
}

impl Help {
    fn individual_help(&self, input: &str, cmd: Arc<dyn Command>, module: Arc<Module>) -> Option<EmbedBuilder> {
        let options = &self.1;

        let cmd_options = cmd.options();
        let mut aliases = Vec::new();
        let mut name = input.to_string();
        
        for (k, v) in module.commands.iter() {
            match v {
                CommandOrAlias::Alias(alias) => {
                    if alias == &input.to_lowercase() {
                        aliases.push(k);
                    }
                },
                CommandOrAlias::Command(_) => {
                    if k == &input.to_lowercase() {
                        name = k.clone();
                    }
                },
            }
        }

        if !cmd_options.help_available { return None }

        let mut embed = EmbedBuilder::new()
            .title(name.clone())
            .color(colors::MAIN);

        if let Some(description) = &cmd_options.description {
            embed = embed.field(EmbedFieldBuilder::new(options.description_label.clone(), description));
        }

        let restrictions = format!("Guild Only: {}\nOwner Only: {}",
            cmd_options.guild_only,
            cmd_options.owner_only);
        embed = embed.field(EmbedFieldBuilder::new(options.restrictions_label.clone(), restrictions).inline());

        if !cmd_options.required_permissions.is_empty() {
            embed = embed.field(EmbedFieldBuilder::new(options.required_permissions_label.clone(), format!("{:?}", cmd_options.required_permissions)).inline());
        }

        if let Some(usage) = &cmd_options.usage {
            embed = embed.field(EmbedFieldBuilder::new(options.usage_label.clone(), usage));
        }

        
        if !aliases.is_empty() {
            let aliases = aliases.iter().map(|e| e.as_str()).collect::<Vec<&str>>().join(", ");
            embed = embed.field(EmbedFieldBuilder::new(options.aliases_label.clone(), aliases));
        }

        if !cmd_options.examples.is_empty() {
            let examples = format!("{} {}",
                name,
                cmd_options.examples.iter()
                    .map(|e| e.as_str()).collect::<Vec<&str>>()
                    .join(format!("\n{}", name).as_str())
            );
            embed = embed.field(EmbedFieldBuilder::new(options.examples_label.clone(), examples));
        }

        Some(embed)
    }
}

#[async_trait]
impl Command for Help {
    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn StdError + Send + Sync>> {
        let modules = &self.0;
        let options = &self.1;

        if let Ok(input) = args.single::<String>() {
            let mut found = false;

            for (module_name, module) in modules.iter() {
                if let Some(ref prefix) = module.prefix {
                    if prefix == &input.to_lowercase() {
                        if let Ok(subcmd) = args.single::<String>() {
                            let name = format!("{} {}", input, &subcmd);
                            if let Some(cmd) = super::command_crawl(subcmd, module) {
                                if let Some(embed) = self.individual_help(&name, cmd, module.clone()) {
                                    ctx.http.create_message(message.channel_id)
                                        .reply(message.id)
                                        .embed(embed.build()?)?
                                        .await?;

                                    found = true;
                                }
                            }
                        } else {
                            //module matches, no subcommand
                            let mut commands: Vec<&str> = module.commands.iter()
                                .filter_map(|(name, cmd)| { match cmd {
                                    CommandOrAlias::Command(_) => Some(name.as_str()),
                                    CommandOrAlias::Alias(_) => None
                                }}).collect();
                            commands.sort_unstable();

                            let embed = EmbedBuilder::new()
                                .title(module_name.clone())
                                .color(colors::MAIN)
                                .field(EmbedFieldBuilder::new("Sub-commands", commands.join("\n ")))
                                .build()?;

                            ctx.http.create_message(message.channel_id).reply(message.id)
                                .embed(embed)?
                                .await?;

                            found = true;
                        }
                    }
                }
                if let Some(cmd) = super::command_crawl(input.clone(), module) {
                    if let Some(embed) = self.individual_help(&input, cmd, module.clone()) {
                        ctx.http.create_message(message.channel_id)
                            .reply(message.id)
                            .embed(embed.build()?)?
                            .await?;

                        found = true;
                    }
                }
            }

            if !found {
                ctx.http.create_message(message.channel_id)
                    .reply(message.id)
                    .content(format!("**Error**: Command `{}` not found.", input))?
                    .await?;
            }
        } else {
            let mut embed = EmbedBuilder::new()
                .description(options.individual_command_tip.clone())
                .color(colors::MAIN);

            for (name, module) in modules.iter() {
                let name = if let Some(ref prefix) = module.prefix {
                    format!("{} (prefix: `{}`)", name, prefix)
                } else { name.clone() };

                let mut commands: Vec<&str> = module.commands.iter()
                    .filter_map(|(k, v)|  match v {
                        CommandOrAlias::Command(cmd) => { if cmd.options().help_available {
                            Some(k.as_str()) } else { None }
                        }
                        CommandOrAlias::Alias(_) => { None }
                    }).collect();
                commands.sort_unstable();
                
                if commands.is_empty() { continue; }

                let field = EmbedFieldBuilder::new(name, format!("`{}`", commands.join("`, `")));

                embed = embed.field(field);
            }

            ctx.http.create_message(message.channel_id)
                .reply(message.id)
                .embed(embed.build()?)?
                .await?;
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct HelpOptions {
    pub suggestion_text: String,
    pub no_help_available_text: String,
    pub usage_label: String,
    pub examples_label: String,
    pub description_label: String,
    pub aliases_label: String,
    pub guild_only_text: String,
    pub available_text: String,
    pub command_not_found_text: String,
    pub individual_command_tip: String,
    pub striked_commands_tip_in_dm: Option<String>,
    pub striked_commands_tip_in_guild: Option<String>,
    pub group_prefix: String,
    pub restrictions_label: String,
    pub required_permissions_label: String,
    // pub lacking_role: HelpBehaviour,
    // pub lacking_permissions: HelpBehaviour,
    // pub wrong_channel: HelpBehaviour,
    // pub embed_error_colour: Colour,
    // pub embed_success_colour: Colour,
    pub max_levenshtein_distance: usize,
}

impl Default for HelpOptions {
    fn default() -> HelpOptions {
        HelpOptions {
            suggestion_text: "Did you mean `{}`?".to_string(),
            no_help_available_text: "**Error**: No help available.".to_string(),
            usage_label: "Usage".to_string(),
            examples_label: "Examples".to_string(),
            aliases_label: "Aliases".to_string(),
            description_label: "Description".to_string(),
            guild_only_text: "Only in guilds".to_string(),
            restrictions_label: "Restrictions".to_string(),
            required_permissions_label: "Required Permissions".to_string(),
            available_text: "Available".to_string(),
            command_not_found_text: "**Error**: Command `{}` not found.".to_string(),
            individual_command_tip: "To get help with an individual command, pass its \
                 name as an argument to this command.".to_string(),
            group_prefix: "Prefix".to_string(),
            striked_commands_tip_in_dm: Some(String::new()),
            striked_commands_tip_in_guild: Some(String::new()),
            // lacking_role: HelpBehaviour::Strike,
            // lacking_permissions: HelpBehaviour::Strike,
            // wrong_channel: HelpBehaviour::Strike,
            // embed_error_colour: Colour::DARK_RED,
            // embed_success_colour: Colour::ROSEWATER,
            max_levenshtein_distance: 0,
        }
    }
}