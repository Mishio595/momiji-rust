use crate::Context;
use std::collections::HashMap;
use std::{fmt, fmt::{Debug, Formatter}};
use std::error::Error as StdError;
use std::sync::Arc;
use super::args::Args;
use twilight_model::channel::Message;
use twilight_model::guild::Permissions;

#[derive(Clone, Debug)]
pub struct Error(pub String);

// TODO: Have separate `From<(&)String>` and `From<&str>` impls via specialization
impl<D: fmt::Display> From<D> for Error {
    fn from(d: D) -> Self {
        Error(d.to_string())
    }
}

pub enum CommandOrAlias {
    Alias(String),
    Command(Arc<dyn Command>),
}

impl Debug for CommandOrAlias {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match *self {
            CommandOrAlias::Alias(ref s) => f.debug_tuple("CommandOrAlias::Alias").field(&s).finish(),
            CommandOrAlias::Command(ref arc) => f.debug_tuple("CommandOrAlias::Command").field(&arc.options()).finish(),
        }
    }
}

#[derive(Debug)]
pub struct ModuleBuilder {
    prefix: Option<String>,
    commands: HashMap<String, CommandOrAlias>,
    required_permissions: Permissions,
    help_available: bool,
    guild_only: bool,
    owners_only: bool,
    default_command: Option<Arc<dyn Command>>,
    description: Option<String>,
}

impl Default for ModuleBuilder {
    fn default() -> Self {
        Self {
            prefix: None,
            commands: HashMap::new(),
            required_permissions: Permissions::empty(),
            guild_only: false,
            help_available: true,
            owners_only: false,
            default_command: None,
            description: None,
        }
    }
}

impl ModuleBuilder {
    pub fn build(self) -> Module {
        Module {
            prefix: self.prefix,
            commands: self.commands,
            required_permissions: self.required_permissions,
            guild_only: self.guild_only,
            help_available: self.help_available,
            owners_only: self.owners_only,
            default_command: self.default_command,
            description: self.description,
        }
    }

    pub fn prefix<T: Into<String>>(mut self, prefix: T) -> Self {
        self.prefix = Some(prefix.into());

        self
    }

    pub fn add_command<T: Into<String>>(mut self, name: T, command: Arc<dyn Command>) -> Self {
        self.commands.insert(name.into(), CommandOrAlias::Command(command));

        self
    }

    pub fn add_alias<T: Into<String>>(mut self, name: T, alias: T) -> Self {
        self.commands.insert(name.into(), CommandOrAlias::Alias(alias.into()));

        self
    }

    pub fn required_permissions(mut self, p: Permissions) -> Self {
        self.required_permissions = p;

        self
    }

    pub fn guild_only(mut self, b: bool) -> Self {
        self.guild_only = b;

        self
    }

    pub fn help_available(mut self, b: bool) -> Self {
        self.help_available = b;

        self
    }

    pub fn owners_only(mut self, b: bool) -> Self {
        self.owners_only = b;

        self
    }

    pub fn default_command(mut self, c: Arc<dyn Command>) -> Self {
        self.default_command = Some(c);

        self
    }

    pub fn description(mut self, d: String) -> Self {
        self.description = Some(d);

        self
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct Module {
    pub prefix: Option<String>,
    pub commands: HashMap<String, CommandOrAlias>,
    pub required_permissions: Permissions,
    pub help_available: bool,
    pub guild_only: bool,
    pub owners_only: bool,
    pub default_command: Option<Arc<dyn Command>>,
    pub description: Option<String>,
}

impl Module {
    pub(crate) fn builder() -> ModuleBuilder {
        ModuleBuilder::default()
    }
}

#[derive(Debug)]
pub struct Options {
    pub description: Option<String>,
    pub usage: Option<String>,
    pub examples: Vec<String>,
    pub required_permissions: Permissions,
    pub guild_only: bool,
    pub owner_only: bool,
    pub help_available: bool,
}

impl Default for Options {
    fn default() -> Self {
        Self {
            description: None,
            usage: None,
            examples: Vec::new(),
            required_permissions: Permissions::empty(),
            guild_only: false,
            owner_only: false,
            help_available: true,
        }
    }
}

impl From<OptionsBuilder> for Options {
    fn from(builder: OptionsBuilder) -> Self {
        Self {
            description: builder.description,
            usage: builder.usage,
            examples: builder.examples,
            required_permissions: builder.required_permissions,
            guild_only: builder.guild_only,
            owner_only: builder.owner_only,
            help_available: builder.help_available,
        }
    }
}

impl Options {
    pub fn builder() -> OptionsBuilder {
        OptionsBuilder::new()
    }
}

#[derive(Debug)]
pub struct OptionsBuilder {
    description: Option<String>,
    usage: Option<String>,
    examples: Vec<String>,
    required_permissions: Permissions,
    guild_only: bool,
    owner_only: bool,
    help_available: bool,
}

impl OptionsBuilder {
    pub fn new() -> Self {
        OptionsBuilder::default()
    }

    pub fn description<S: Into<String>>(self, description: S) -> Self {
        self._description(description.into())
    }

    fn _description(mut self, description: String) -> Self {
        self.description = Some(description);

        self
    }

    pub fn usage<S: Into<String>>(self, usage: S) -> Self {
        self._usage(usage.into())
    }

    fn _usage(mut self, usage: String) -> Self {
        self.usage = Some(usage);

        self
    }

    pub fn example<S: Into<String>>(self, example: S) -> Self {
        self._example(example.into())
    }

    fn _example(mut self, example: String) -> Self {
        self.examples.push(example);

        self
    }

    pub fn permissions(mut self, permissions: Permissions) -> Self {
        self.required_permissions = permissions;

        self
    }

    pub fn guild_only(mut self, which: bool) -> Self {
        self.guild_only = which;

        self
    }

    pub fn owner_only(mut self, which: bool) -> Self {
        self.owner_only = which;

        self
    }

    pub fn help_available(mut self, which: bool) -> Self {
        self.help_available = which;

        self
    }

    pub fn build(self) -> Options {
        self.into()
    }
}

impl Default for OptionsBuilder {
    fn default() -> Self {
        Self {
            description: None,
            usage: None,
            examples: Vec::new(),
            required_permissions: Permissions::empty(),
            guild_only: false,
            owner_only: false,
            help_available: true,
        }
    }
}

#[async_trait]
pub trait Command: Send + Sync + 'static {
    async fn run(&self, message: Message, args: Args, ctx: Context)
        -> Result<(), Box<dyn StdError + Send + Sync>>;

    fn options(&self) -> Arc<Options> {
        Arc::new(Options::default())
    }

    fn before(&self, _: Message, _: Args, _: Context) -> bool { true }

    fn after(&self, _: Message, _: Args, _: Context, _: Box<dyn StdError + Send + Sync>) {}
}

#[async_trait]
impl Command for Arc<dyn Command> {
    async fn run(&self, m: Message, a: Args, ctx: Context)
        -> Result<(), Box<dyn StdError + Send + Sync>> {
            (**self).run(m, a, ctx).await
        }
    
    fn options(&self) -> Arc<Options> {
        (**self).options()
    }

    fn before(&self, m: Message, a: Args, ctx: Context) -> bool {
        (**self).before(m, a, ctx)
    }

    fn after(&self, m: Message, a: Args, ctx: Context, e: Box<dyn StdError + Send + Sync>) {
        (**self).after(m, a, ctx, e)
    }
}

impl std::fmt::Debug for dyn Command {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Command {:?}", self.options()))
    }
}