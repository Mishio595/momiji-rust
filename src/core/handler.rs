use chrono::Utc;
use crate::Context;
use crate::core::consts::*;
use crate::core::utils::*;
use crate::framework::Framework;
use futures::stream::{Stream, StreamExt};
use levenshtein::levenshtein;
use tracing::{event, Level};
use twilight_mention::Mention;
use std::error::Error;
use std::sync::Arc;
use twilight_cache_inmemory::model::{CachedMember, CachedMessage};
use twilight_embed_builder::{EmbedBuilder, EmbedFooterBuilder, ImageSource};
use twilight_gateway::Event;
use twilight_model::id::{UserId};

use super::utils::build_welcome_embed;
use super::utils::parse_welcome_items;

type EventStream = Box<dyn Stream<Item = (u64, Event)> + Send + Sync + Unpin + 'static>;

pub struct EventHandler {
    framework: Arc<Framework>,
    events: EventStream,
    ctx: Context,
}

impl EventHandler {
    pub fn new(framework: Framework, events: EventStream, ctx: Context) -> Self {
        Self {
            framework: Arc::new(framework),
            events,
            ctx
        }
    }
 
    pub async fn start(&mut self) {
        let events = self.events.as_mut();
        while let Some((shard_id, event)) = events.next().await {
            let mut old_message = None;
            let mut old_member = None;
            match &event {
                Event::MessageDelete(message) => {
                    if let Some(message) = self.ctx.cache.message(message.channel_id, message.id) {
                        old_message = Some(message);
                    }
                },
                Event::MessageUpdate(message) => {
                    if let Some(message) = self.ctx.cache.message(message.channel_id, message.id) {
                        old_message = Some(message);
                    }
                },
                Event::MemberUpdate(member) => {
                    if let Some(member) = self.ctx.cache.member(member.guild_id, member.user.id) {
                        old_member = Some(member);
                    }
                },
                _ => {}
            }

            self.ctx.standby.process(&event);
            self.ctx.cache.update(&event);
    
            tokio::spawn(handle_event(shard_id, event, self.ctx.clone(), self.framework.clone(), old_message, old_member));
        }
    }
}

async fn handle_event(
    shard_id: u64,
    event: Event,
    ctx: Context,
    framework: Arc<Framework>,
    old_message: Option<CachedMessage>,
    old_member: Option<CachedMember>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let (cache, db, http) = {
        let c = ctx.clone();
        (c.cache, c.db, c.http)
    };
    match event {
        Event::MessageCreate(message) => {
            let channel_id = message.channel_id;
            let message_id = message.id;
            if let Err(e) = (*framework).handle_command(message.0, ctx.clone()).await {
                ctx.http.create_message(channel_id)
                    .reply(message_id)
                    .content(format!("{}", e))?
                    .await?;
            }
        }
        Event::MessageDelete(message) => {
            if let Some(guild_id) = message.guild_id {
                let channel_name = cache.guild_channel(message.channel_id)
                    .map(|c| c.name().to_string())
                    .unwrap_or_default();
                let guild_data = db.get_guild(guild_id)?;
                if guild_data.logging.contains(&String::from("message_delete")) { return Ok(()); }
                let audit_channel = guild_data.audit_channel;
                if guild_data.audit && audit_channel.0 > 0 {
                    if let Some(cached_message) = old_message {
                        if let Some(author) = cache.user(cached_message.author) {
                            if author.bot { return Ok(()); }
                            let embed = EmbedBuilder::new()
                                .title("Message Deleted")
                                .color(colors::RED)
                                .footer(EmbedFooterBuilder::new(format!("ID: {}", message.id.0)))
                                .description(format!("**Author:** {}#{} ({}) - {}\n**Channel:** {} ({}) - <#{}>\n**Content:**\n{}",
                                    author.name,
                                    author.discriminator,
                                    author.id.0,
                                    author.mention(),
                                    channel_name,
                                    message.channel_id.0,
                                    message.channel_id.0,
                                    cached_message.content))
                                .timestamp(chrono::Utc::now().to_rfc3339())
                                .build()?;
                            
                            http.create_message(audit_channel).embed(embed)?.await?;
                        }
                    } else {
                        let embed = EmbedBuilder::new()
                            .title("Uncached Message Deleted")
                            .color(colors::RED)
                            .footer(EmbedFooterBuilder::new(format!("ID: {}", message.id.0)))
                            .description(format!("**Channel:** {} ({}) - <#{}>",
                                channel_name,
                                message.channel_id.0,
                                message.channel_id.0,))
                            .timestamp(chrono::Utc::now().to_rfc3339())
                            .build()?;

                        http.create_message(audit_channel).embed(embed)?.await?;
                    }
                }
            }
        }
        Event::MessageUpdate(message) => {
            if message.author.clone().map(|u| u.bot).unwrap_or(false) { return Ok(()) }
            if message.edited_timestamp.is_none() { return Ok(()) }
            if let Some(old_message) = old_message {
                if let Some(guild_id) = message.guild_id {
                    let channel_name = cache.guild_channel(message.channel_id)
                        .map(|c| c.name().to_string())
                        .unwrap_or_default();
                    if let Ok(guild_data) = db.get_guild(guild_id) {
                        if guild_data.logging.contains(&String::from("message_edit")) { return Ok(()) }
                        let audit_channel = guild_data.audit_channel;
                        let new_content = message.content.unwrap_or_default();
                        let distance = levenshtein(old_message.content.as_str(), new_content.as_str());
                        if guild_data.audit && audit_channel.0 > 0 && distance >= guild_data.audit_threshold as usize {
                            let (author_tag, author_mention) = if let Some(user) = message.author {
                                let tag = format!("{}#{}", user.name, user.discriminator);
                                (tag, user.mention().to_string())
                            } else if let Some(user) = cache.user(old_message.author) {
                                let tag = format!("{}#{}", user.name, user.discriminator);
                                (tag, user.mention().to_string())
                            } else {
                                ("Unknown".to_string(), "Unknown".to_string())
                            };
                            let embed = EmbedBuilder::new()
                                .title("Message Edited")
                                .color(colors::MAIN)
                                .timestamp(Utc::now().to_rfc3339())
                                .footer(EmbedFooterBuilder::new(format!("ID: {}", message.id.0)))
                                .description(format!("**Author:** {} ({}) - {}\n**Channel:** {} ({}) - <#{}>\n**Old Content:**\n{}\n**New Content:**\n{}",
                                    author_tag,
                                    old_message.author.0,
                                    author_mention,
                                    channel_name,
                                    message.channel_id.0,
                                    message.channel_id.0,
                                    old_message.content,
                                    new_content
                                    ))
                                .build()?;
                                http.create_message(audit_channel)
                                    .embed(embed)?
                                    .await?;
                        }
                    }
                }
            }
        }
        Event::ShardConnected(_) => {
            event!(Level::DEBUG, "Connected on shard {}", shard_id);
        }
        // TODO join/leave log. Need solution to restart spam, maybe compare to ready
        Event::GuildCreate(guild) => {
            event!(Level::DEBUG, "Guild received: {} ({})", guild.name, guild.id);
            if let Err(why) = db.new_guild(guild.id) {
                event!(Level::DEBUG, "Failed to create guild: {}", why);
            }
        }
        Event::GuildDelete(guild) => {
            if let Err(why) = db.del_guild(guild.id) {
                event!(Level::DEBUG, "Failed to delete {}: {}", guild.id, why)
            }
        }
        Event::MemberAdd(member) => {
            // TODO maybe hackbans still. Think about it
            if let Ok(guild_data) = db.get_guild(member.guild_id) {
                if guild_data.logging.contains(&String::from("member_join")) { return Ok(()) }
                let user_update = crate::db::models::UserUpdate {
                    id: member.user.id.0 as i64,
                    guild_id: member.guild_id.0 as i64,
                    username: member.user.name.clone()
                };
                if let Ok(mut user_data) = db.upsert_user(user_update) {
                    if guild_data.audit && guild_data.audit_channel.0 > 0 {
                        let audit_channel = guild_data.audit_channel;
                        let embed = EmbedBuilder::new()
                            .title("Member Joined")
                            .color(colors::GREEN)
                            .thumbnail(ImageSource::url(user_avatar_url(&member.user))?)
                            .timestamp(Utc::now().to_rfc3339())
                            .description(format!("<@{}>\n{}\n{}", member.user.id.0, format!("{}#{}", member.user.name, member.user.discriminator), member.user.id.0))
                            .build()?;
                        http.create_message(audit_channel).embed(embed)?.await?;
                    }
                    if guild_data.welcome && guild_data.welcome_channel.0 > 0 {
                        let channel_id = guild_data.welcome_channel;
                        if guild_data.welcome_type.as_str() == "embed" {
                            let embed = build_welcome_embed(guild_data.welcome_message, &member, ctx)?.build()?;
                            http.create_message(channel_id).embed(embed)?.await?;
                        } else {
                            let content = parse_welcome_items(guild_data.welcome_message, &member, ctx);
                            http.create_message(channel_id).content(content)?.await?;
                        }
                    }
                    if guild_data.autorole && !guild_data.autoroles.is_empty() {
                        for id in guild_data.autoroles.into_iter() {
                            http.add_guild_member_role(member.guild_id, member.user.id, id).await?;
                        }
                    }
                    user_data.username = format!("{}#{}", member.user.name, member.user.discriminator);
                    user_data.nickname = member.nick.clone().unwrap_or_else(|| member.user.name.clone());
                    user_data.roles = member.roles.clone();
                    let _ = db.update_user(member.user.id, member.guild_id, user_data);
                }
            }
        }
        Event::MemberRemove(member) => {
            if let Ok(guild_data) = db.get_guild(member.guild_id) {
                let _ = db.del_user(member.user.id, member.guild_id);
                if guild_data.logging.contains(&String::from("member_leave")) { return Ok(()) }
                if guild_data.audit && guild_data.audit_channel.0 > 0 {
                    let audit_channel = guild_data.audit_channel;
                    let embed = EmbedBuilder::new()
                        .title("Member Left")
                        .color(colors::RED)
                        .thumbnail(ImageSource::url(user_avatar_url(&member.user))?)
                        .timestamp(Utc::now().to_rfc3339())
                        .description(format!("<@{}>\n{}#{}\n{}", member.user.id.0, member.user.name, member.user.discriminator , member.user.id))
                        .build()?;
                    http.create_message(audit_channel).embed(embed)?.await?;
                }
                //TODO kick log
            }
        }
        Event::MemberUpdate(member) => {
            if let Some(old_member) = old_member {
                if let Ok(guild_data) = ctx.db.get_guild(member.guild_id) {
                    if guild_data.audit && guild_data.audit_channel.0 > 0 {
                        let audit_channel = guild_data.audit_channel;
                        if !guild_data.logging.contains(&String::from("nickname_change")) {
                            if member.nick != old_member.nick {
                                let old_nick = old_member.nick.clone().unwrap_or_default();
                                let new_nick = member.nick.clone().unwrap_or_default();
                                let embed = EmbedBuilder::new()
                                    .title("Nickname Changed")
                                    .color(colors::MAIN)
                                    .thumbnail(ImageSource::url(user_avatar_url(&member.user))?)
                                    .description(format!("**User: ** {}#{}\n**Old:** {}\n**New:** {}",
                                        member.user.name,
                                        member.user.discriminator,
                                        old_nick,
                                        new_nick
                                    )).build()?;

                                ctx.http.create_message(audit_channel).embed(embed)?.await?;
                            }
                            if !guild_data.logging.contains(&String::from("role_change")) {
                                let mut roles_added = member.roles.clone();
                                roles_added.retain(|r| !old_member.roles.contains(r));
                                if !roles_added.is_empty() {
                                    let guild_roles = ctx.http.roles(member.guild_id).await?;
                                    let roles_added: Vec<String> = guild_roles.into_iter()
                                        .filter_map(|role| if roles_added.contains(&role.id) { Some(role.name) } else { None })
                                        .collect();
                                    let embed = EmbedBuilder::new()
                                        .title("Roles Changed")
                                        .color(colors::MAIN)
                                        .thumbnail(ImageSource::url(user_avatar_url(&member.user))?)
                                        .description(format!("**User: ** {}#{}\n**Added:** {}",
                                            member.user.name,
                                            member.user.discriminator,
                                            roles_added.join(", ")
                                        )).build()?;
                                    
                                    ctx.http.create_message(audit_channel).embed(embed)?.await?;
                                }
                                let mut roles_removed = old_member.roles.clone();
                                roles_removed.retain(|r| !member.roles.contains(r));
                                if !roles_removed.is_empty() {
                                    let guild_roles = ctx.http.roles(member.guild_id).await?;
                                    let roles_removed: Vec<String> = guild_roles.into_iter()
                                        .filter_map(|role| if roles_removed.contains(&role.id) { Some(role.name) } else { None })
                                        .collect();
                                    let embed = EmbedBuilder::new()
                                        .title("Roles Changed")
                                        .color(colors::MAIN)
                                        .thumbnail(ImageSource::url(user_avatar_url(&member.user))?)
                                        .description(format!("**User: ** {}#{}\n**Removed:** {}",
                                            member.user.name,
                                            member.user.discriminator,
                                            roles_removed.join(", ")
                                        )).build()?;
                                    
                                    ctx.http.create_message(audit_channel).embed(embed)?.await?;
                                }
                            }
                        }
                    }
                }
            }
        }
        Event::BanAdd(ban) => {
            use twilight_model::guild::audit_log::AuditLogEvent;
            let audit_request = ctx.http.audit_log(ban.guild_id)
                .action_type(AuditLogEvent::MemberBanAdd)
                .limit(1)?;
            if let Some(audit_log) = audit_request.await? {
                if let Some(audit) = audit_log.audit_log_entries.first() {
                    if let Ok(guild_data) = ctx.db.get_guild(ban.guild_id) {
                        if guild_data.logging.contains(&String::from("member_ban")) { return Ok(()) }
                        let target_id = audit.target_id.clone()
                            .map(|ref s| UserId(s.parse::<u64>().unwrap_or(0)))
                            .unwrap();
                        if guild_data.modlog && guild_data.modlog_channel.0 > 0 && target_id == ban.user.id {
                            let modlog_channel = guild_data.modlog_channel;
                            let moderator = match audit.user_id {
                                Some(user_id) => { match ctx.http.user(user_id).await? {
                                    Some(user) => { format!("{}#{} ({})", user.name, user.discriminator, user.id.0) }
                                    None => { "unknown".to_string() }
                                }}
                                None => { "unknown".to_string() }
                            };
                            let embed = EmbedBuilder::new()
                                .title("Member Banned")
                                .color(colors::RED)
                                .thumbnail(ImageSource::url(user_avatar_url(&ban.user))?)
                                .timestamp(chrono::Utc::now().to_rfc3339())
                                .description(format!("**Member:** {}#{} ({}) - {}\n**Responsible Moderator:** {}\n**Reason:** {}",
                                    ban.user.name,
                                    ban.user.discriminator,
                                    ban.user.id.0,
                                    ban.user.mention(),
                                    moderator,
                                    audit.reason.clone().unwrap_or_else(|| "None".to_string())
                                )).build()?;
                            ctx.http.create_message(modlog_channel).embed(embed)?.await?;
                        }
                    }
                }
            }
        }
        Event::BanRemove(ban) => {
            use twilight_model::guild::audit_log::AuditLogEvent;
            let audit_request = ctx.http.audit_log(ban.guild_id)
                .action_type(AuditLogEvent::MemberBanRemove)
                .limit(1)?;
            if let Some(audit_log) = audit_request.await? {
                if let Some(audit) = audit_log.audit_log_entries.first() {
                    if let Ok(guild_data) = ctx.db.get_guild(ban.guild_id) {
                        if guild_data.logging.contains(&String::from("member_unban")) { return Ok(()) }
                        let target_id = audit.target_id.clone()
                            .map(|ref s| UserId(s.parse::<u64>().unwrap_or(0)))
                            .unwrap();
                        if guild_data.modlog && guild_data.modlog_channel.0 > 0 && target_id == ban.user.id {
                            let modlog_channel = guild_data.modlog_channel;
                            let moderator = match audit.user_id {
                                Some(user_id) => { match ctx.http.user(user_id).await? {
                                    Some(user) => { format!("{}#{} ({})", user.name, user.discriminator, user.id.0) }
                                    None => { "unknown".to_string() }
                                }}
                                None => { "unknown".to_string() }
                            };
                            let embed = EmbedBuilder::new()
                                .title("Member Unbanned")
                                .color(colors::GREEN)
                                .thumbnail(ImageSource::url(user_avatar_url(&ban.user))?)
                                .timestamp(chrono::Utc::now().to_rfc3339())
                                .description(format!("**Member:** {}#{} ({}) - {}\n**Responsible Moderator:** {}\n**Reason:** {}",
                                    ban.user.name,
                                    ban.user.discriminator,
                                    ban.user.id.0,
                                    ban.user.mention(),
                                    moderator,
                                    audit.reason.clone().unwrap_or_else(|| "None".to_string())
                                )).build()?;
                            ctx.http.create_message(modlog_channel).embed(embed)?.await?;
                        }
                    }
                }
            }
        }
        Event::Ready(ready) => {
            event!(Level::DEBUG, "Connected with session_id {}", ready.session_id);
        }
        Event::Resumed => {
            event!(Level::DEBUG, "Session resumed");
        }
        _ => { #[cfg(debug_assertions)] { event!(Level::DEBUG, "Unhandled event: {:?}", event.kind()); }}
    }

    Ok(())
}