//! A set of abstractions for manipulating a PgSQL database relevant to Momiji's stored data.
pub mod models;
mod schema;
mod r#impl;

use self::models::*;
use self::schema::*;
use chrono::offset::Utc;
use diesel::pg::PgConnection;
use diesel::pg::upsert::excluded;
use diesel::prelude::*;
use diesel::r2d2::{
    ConnectionManager,
    Pool,
    PooledConnection
};
use diesel;
use std::env;
use std::ops::Deref;
use std::sync::Arc;

/// While the struct itself and the connection are public, Database cannot be manually
/// instantiated. Use Database::connect() to start it.
#[derive(Clone)]
#[non_exhaustive]
pub struct DatabaseConnection {
    pub pool: Arc<Pool<ConnectionManager<PgConnection>>>,
}

impl DatabaseConnection {
    /// Create a new database with a connection.
    /// Returns a new Database.
    pub fn connect() -> Self {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let manager = ConnectionManager::<PgConnection>::new(database_url);
        let pool = Pool::builder()
            .max_size(10)
            .build(manager)
            .expect("Failed to make connection pool");

        Self {
            pool: Arc::new(pool),
        }
    }

    /// Request a connection from the connection pool
    fn conn(&self) -> PooledConnection<ConnectionManager<PgConnection>> {
        self.pool.clone().get().expect("Attempt to get connection timed out")
    }

    // Guild Tools
    /// Add a guild with a given ID.
    /// Returns the Ok(Some(Guild)) on success or Ok(None) if there is a conflict.
    /// May return Err(DatabaseError) in the event of some other failure.
    pub fn new_guild<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<Option<Guild>> {
		self._new_guild(guild_id.into().into()).map(|g| g.map(|g| g.into()))
	}

	fn _new_guild(&self, id: i64) -> QueryResult<Option<InternalGuild>> {
        let guild = NewGuild {
            id,
        };
        diesel::insert_into(guilds::table)
            .values(&guild)
            .on_conflict_do_nothing()
            .get_result(self.conn().deref())
            .optional()
    }
    /// Add multiple guilds with a vector of IDs
    /// Does nothing on conflict
    /// Returns Result<count, err>
    pub fn new_guilds<It, G>(&self, guild_ids: It) -> QueryResult<usize>
        where It: IntoIterator<Item = G>,
            G: Into<InternalGuildId> {
        let guild_ids = guild_ids.into_iter().map(|id| id.into().into());
		self._new_guilds(guild_ids)
	}

	fn _new_guilds<T: IntoIterator<Item = i64>>(&self, ids: T) -> QueryResult<usize> {
        let guilds = {
            ids.into_iter().map(|id| {
                NewGuild {
                    id,
                }
            }).collect::<Vec<NewGuild>>()
        };
        diesel::insert_into(guilds::table)
            .values(&guilds)
            .on_conflict_do_nothing()
            .execute(self.conn().deref())
    }
    /// Delete a guild by the ID.
    /// Returns Result<guild_id, err>
    pub fn del_guild<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<i64> {
		self._del_guild(guild_id.into().into())
	}

	fn _del_guild(&self, g_id: i64) -> QueryResult<i64> {
        use crate::db::schema::guilds::columns::id;
        diesel::delete(guilds::table)
            .filter(id.eq(&g_id))
            .returning(id)
            .get_result(self.conn().deref())
    }
    /// Select a guild
    /// Returns Result<Guild, Err>
    pub fn get_guild<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<Guild> {
		self._get_guild(guild_id.into().into())
            .map(|g| g.into())
	}

	fn _get_guild(&self, g_id: i64) -> QueryResult<InternalGuild> {
        guilds::table.find(&g_id)
            .first(self.conn().deref())
    }
    /// Update a guild
    /// Returns Result<Guild, Err>
    pub fn update_guild<G: Into<InternalGuildId>>(&self, guild_id: G, guild: Guild) -> QueryResult<Guild> {
		self._update_guild(guild_id.into().into(), guild.into())
            .map(|g| g.into())
	}

	fn _update_guild(&self, g_id: i64, guild: InternalGuild) -> QueryResult<InternalGuild> {
        let target = guilds::table.find(&g_id);
        diesel::update(target)
            .set(&guild)
            .get_result(self.conn().deref())
    }
    /// Get the count of guilds in the database
    pub fn count_guilds(&self) -> QueryResult<i64> {
		self._count_guilds()
	}

	fn _count_guilds(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        guilds::table.select(count_star())
            .get_result(self.conn().deref())
    }

    // User Tools
    /// Add a user with a given user ID and guild ID.
    /// Returns the User on success.
    pub fn new_user<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<User<Utc>>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._new_user(user_id.into().into(), guild_id.into().into())
            .map(|u| u.into())
	}

	fn _new_user(&self, id: i64, guild_id: i64) -> QueryResult<InternalUser<Utc>> {
        let user = NewUser {
           id,
           guild_id,
        };
        diesel::insert_into(users::table)
            .values(&user)
            .get_result(self.conn().deref())
    }
    /// Delete a user by user ID and guild ID.
    /// Returns the ID on success.
    pub fn del_user<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<i64>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._del_user(user_id.into().into(), guild_id.into().into())
	}

	fn _del_user(&self, u_id: i64, g_id: i64) -> QueryResult<i64> {
        use crate::db::schema::users::columns::{id, guild_id};
        diesel::delete(users::table)
            .filter(id.eq(&u_id))
            .filter(guild_id.eq(&g_id))
            .returning(id)
            .get_result(self.conn().deref())
    }
    /// Select a user
    /// Returns the user on success
    pub fn get_user<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<User<Utc>>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._get_user(user_id.into().into(), guild_id.into().into())
            .map(|u| u.into())
	}

	fn _get_user(&self, u_id: i64, g_id: i64) -> QueryResult<InternalUser<Utc>> {
        users::table.find((u_id, g_id))
            .first(self.conn().deref())
    }
    /// Select all users in a guild
    /// Returns a vector of users on success
    pub fn get_users<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<Vec<User<Utc>>> {
		self._get_users(guild_id.into().into())
            .map(|v| v.into_iter()
                .map(|u| u.into())
            .collect())
	}

	fn _get_users(&self, g_id: i64) -> QueryResult<Vec<InternalUser<Utc>>> {
        use crate::db::schema::users::columns::guild_id;
        users::table.filter(guild_id.eq(&g_id))
            .get_results(self.conn().deref())
    }
    /// Update a user
    /// Returns the new user on success
    pub fn update_user<G, U>(&self, user_id: U, guild_id: G, user: User<Utc>) -> QueryResult<User<Utc>>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._update_user(user_id.into().into(), guild_id.into().into(), user.into())
            .map(|u| u.into())
	}

	fn _update_user(&self, u_id: i64, g_id: i64, user: InternalUser<Utc>) -> QueryResult<InternalUser<Utc>> {
        let target = users::table.find((u_id, g_id));
        diesel::update(target)
            .set(&user)
            .get_result(self.conn().deref())
    }
    /// Upsert a user
    /// Returns the new user on success
    pub fn upsert_user(&self, user: UserUpdate) -> QueryResult<User<Utc>> {
		self._upsert_user(user).map(|u| u.into())
	}

	fn _upsert_user(&self, user: UserUpdate) -> QueryResult<InternalUser<Utc>> {
        use crate::db::schema::users::columns::{id, guild_id};
        diesel::insert_into(users::table)
            .values(&user)
            .on_conflict((id, guild_id))
            .do_update()
            .set(&user)
            .get_result(self.conn().deref())
    }
    /// Upserts multiple users with a vector of UserUpdates
    /// Returns Result<count, err>
    pub fn upsert_users(&self, users: &[UserUpdate]) -> QueryResult<usize> {
		self._upsert_users(users)
	}

	fn _upsert_users(&self, users: &[UserUpdate]) -> QueryResult<usize> {
        use crate::db::schema::users::columns::*;
        diesel::insert_into(users::table)
            .values(users)
            .on_conflict((id, guild_id))
            .do_update()
            .set((nickname.eq(excluded(nickname)),
                username.eq(excluded(username)),
                roles.eq(excluded(roles))))
            .execute(self.conn().deref())
    }
    /// Get the count of users in the database
    pub fn count_users(&self) -> QueryResult<i64> {
		self._count_users()
	}

	fn _count_users(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        users::table.select(count_star())
            .get_result(self.conn().deref())
    }

    // Role Tools
    /// Add a role with the given role ID, guild ID, and optionally a category and aliases.
    /// Returns the Role on success.
    pub fn new_role<G, R>(&self, role_id: R, guild_id: G, category: Option<String>, aliases: Option<Vec<String>>) -> QueryResult<Role>
        where G: Into<InternalGuildId>,
            R: Into<InternalRoleId> {
		self._new_role(role_id.into().into(), guild_id.into().into(), category, aliases)
            .map(|r| r.into())
	}

	fn _new_role(&self, id: i64, guild_id: i64, category: Option<String>, aliases: Option<Vec<String>>) -> QueryResult<InternalRole> {
        let role = NewRole {
            id,
            guild_id,
            category,
            aliases,
        };
        diesel::insert_into(roles::table)
            .values(&role)
            .get_result(self.conn().deref())
    }
    /// Delete a role by role ID and guild ID.
    /// Returns the ID on success.
    pub fn del_role<G, R>(&self, role_id: R, guild_id: G) -> QueryResult<i64>
        where G: Into<InternalGuildId>,
            R: Into<InternalRoleId> {
		self._del_role(role_id.into().into(), guild_id.into().into())
	}

	fn _del_role(&self, r_id: i64, g_id: i64) -> QueryResult<i64> {
        use crate::db::schema::roles::columns::{id, guild_id};
        diesel::delete(roles::table)
            .filter(id.eq(&r_id))
            .filter(guild_id.eq(&g_id))
            .returning(id)
            .get_result(self.conn().deref())
    }
    /// Select a role
    /// Returns the role on success
    pub fn get_role<G, R>(&self, role_id: R, guild_id: G) -> QueryResult<Role>
        where G: Into<InternalGuildId>,
            R: Into<InternalRoleId> {
		self._get_role(role_id.into().into(), guild_id.into().into())
            .map(|r| r.into())
	}

	fn _get_role(&self, r_id: i64, g_id: i64) -> QueryResult<InternalRole> {
        roles::table.find((r_id, g_id))
            .first(self.conn().deref())
    }
    /// Select all roles by guild id
    /// Returns a vector of roles on success
    pub fn get_roles<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<Vec<Role>> {
		self._get_roles(guild_id.into().into())
            .map(|v| v.into_iter()
                .map(|r| r.into())
            .collect())
	}

	fn _get_roles(&self, g_id: i64) -> QueryResult<Vec<InternalRole>> {
        use crate::db::schema::roles::columns::guild_id;
        roles::table.filter(guild_id.eq(&g_id))
            .get_results(self.conn().deref())
    }
    /// Update a role
    /// Returns the new role on success
    pub fn update_role<G, R>(&self, role_id: R, guild_id: G, role: Role) -> QueryResult<Role>
        where G: Into<InternalGuildId>,
            R: Into<InternalRoleId> {
		self._update_role(role_id.into().into(), guild_id.into().into(), role.into())
            .map(|r| r.into())
	}

	fn _update_role(&self, r_id: i64, g_id: i64, role: InternalRole) -> QueryResult<InternalRole> {
        let target = roles::table.find((r_id, g_id));
        diesel::update(target)
            .set(&role)
            .get_result(self.conn().deref())
    }
    /// Get the count of roles in the database
    pub fn count_roles(&self) -> QueryResult<i64> {
		self._count_roles()
	}

	fn _count_roles(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        roles::table.select(count_star())
            .get_result(self.conn().deref())
    }

    // Note Tools
    /// Add a note to the given user in the given guild by a given moderator
    /// Returns the Note on success.
    pub fn new_note<G, S, U>(&self, user_id: U, guild_id: G, note: S, moderator: U) -> QueryResult<Note<Utc>>
        where G: Into<InternalGuildId>,
            S: Into<String>,
            U: Into<InternalUserId> {
		self._new_note(user_id.into().into(), guild_id.into().into(), note.into(), moderator.into().into())
            .map(|n| n.into())
	}

	fn _new_note(&self, user_id: i64, guild_id: i64, note: String, moderator: i64) -> QueryResult<InternalNote<Utc>> {
        let note = NewNote {
            user_id,
            guild_id,
            note,
            moderator,
        };
        diesel::insert_into(notes::table)
            .values(&note)
            .get_result(self.conn().deref())
    }
    /// Delete a note by index, user ID, and guild ID.
    /// Returns the Note.note on success.
    pub fn del_note<G, U>(&self, note_id: i32, user_id: U, guild_id: G) -> QueryResult<String>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._del_note(note_id, user_id.into().into(), guild_id.into().into())
	}

	fn _del_note(&self, n_id: i32, u_id: i64, g_id: i64) -> QueryResult<String> {
        use crate::db::schema::notes::columns::{user_id, guild_id, id, note};
        diesel::delete(notes::table)
            .filter(user_id.eq(&u_id))
            .filter(guild_id.eq(&g_id))
            .filter(id.eq(&n_id))
            .returning(note)
            .get_result(self.conn().deref())
    }
    /*
    /// Select a note
    /// Returns the note on success
    pub fn get_note(&self, n_id: i32, u_id: i64, g_id: i64) -> QueryResult<Note<Utc>> {
		self._get_note()
	}

	fn _get_note(&self, n_id: i32, u_id: i64, g_id: i64) -> QueryResult<Note<Utc>> {
        notes::table.find((n_id, u_id, g_id))
            .first(self.conn().deref())
    }*/
    /// Select all notes for a user
    /// Returns a vec of notes on success
    pub fn get_notes<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<Vec<Note<Utc>>>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._get_notes(user_id.into().into(), guild_id.into().into())
            .map(|v| v.into_iter()
                .map(|n| n.into())
            .collect())
	}

	fn _get_notes(&self, u_id: i64, g_id: i64) -> QueryResult<Vec<InternalNote<Utc>>> {
        use crate::db::schema::notes::columns::{user_id, guild_id};
        notes::table.filter(user_id.eq(&u_id))
            .filter(guild_id.eq(&g_id))
            .get_results(self.conn().deref())
    }
    /// Get the count of notes in the database
    pub fn count_notes(&self) -> QueryResult<i64> {
		self._count_notes()
	}

	fn _count_notes(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        notes::table.select(count_star())
            .get_result(self.conn().deref())
    }

    // Timer Tools
    /// Add a timer
    /// Returns the timer on success.
    pub fn new_timer<T: Into<String>>(&self, starttime: i64, endtime: i64, data: T) -> QueryResult<Timer> {
		self._new_timer(starttime, endtime, data.into())
            .map(|t| t.into())
	}

	fn _new_timer(&self, starttime: i64, endtime: i64, data: String) -> QueryResult<InternalTimer> {
        let timer = NewTimer {
            starttime,
            endtime,
            data,
        };
        diesel::insert_into(timers::table)
            .values(&timer)
            .get_result(self.conn().deref())
    }
    /// Delete a timer with the given ID.
    /// Returns the note data on success.
    pub fn del_timer(&self, timer_id: i32) -> QueryResult<String> {
		self._del_timer(timer_id)
	}

	fn _del_timer(&self, t_id: i32) -> QueryResult<String> {
        use crate::db::schema::timers::columns::{id, data};
        diesel::delete(timers::table)
            .filter(id.eq(&t_id))
            .returning(data)
            .get_result(self.conn().deref())
    }
    /*
    /// Select a timer
    /// Returns the timer on success
    pub fn get_timer(&self, t_id: i32) -> QueryResult<Timer> {
		self._get_timer()
	}

	fn _get_timer(&self, t_id: i32) -> QueryResult<Timer> {
        timers::table.find(t_id)
            .first(self.conn().deref())
    }*/
    /// Select all timers
    /// Returns a vec of timers on success
    pub fn get_timers(&self) -> QueryResult<Vec<Timer>> {
		self._get_timers().map(|v| v.into_iter()
            .map(|t| t.into())
            .collect())
	}

	fn _get_timers(&self) -> QueryResult<Vec<InternalTimer>> {
        timers::table.get_results(self.conn().deref())
    }
    /// Get the count of timers in the database
    pub fn count_timers(&self) -> QueryResult<i64> {
		self._count_timers()
	}

	fn _count_timers(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        timers::table.select(count_star())
            .get_result(self.conn().deref())
    }
    /// Get the timer with the closest expiration time to the present
    pub fn get_earliest_timer(&self) -> QueryResult<Timer> {
		self._get_earliest_timer().map(|t| t.into())
	}

	fn _get_earliest_timer(&self) -> QueryResult<InternalTimer> {
        use crate::db::schema::timers::{all_columns, columns::endtime};
        timers::table.select(all_columns)
            .order(endtime.asc())
            .first(self.conn().deref())
    }

    // Case Tools
    /// Add a Case
    /// Returns the Case on success
    pub fn new_case<G, S, U>(&self, user_id: U, guild_id: G, casetype: S, reason: Option<String>, moderator: U) -> QueryResult<Case<Utc>>
        where G: Into<InternalGuildId>,
            S: Into<String>,
            U: Into<InternalUserId> {
		self._new_case(user_id.into().into(), guild_id.into().into(), casetype.into(), reason, moderator.into().into())
            .map(|c| c.into())
	}

	fn _new_case(&self, user_id: i64, guild_id: i64, casetype: String, reason: Option<String>, moderator: i64) -> QueryResult<InternalCase<Utc>> {
        let case = NewCase {
            user_id,
            guild_id,
            casetype,
            reason,
            moderator,
        };
        diesel::insert_into(cases::table)
            .values(&case)
            .get_result(self.conn().deref())
    }
    /*
    /// Delete a case
    /// Returns the case on success.
    pub fn del_case(&self, c_id: i32, u_id: i64, g_id: i64) -> QueryResult<Case<Utc>> {
		self._del_case()
	}

	fn _del_case(&self, c_id: i32, u_id: i64, g_id: i64) -> QueryResult<Case<Utc>> {
        use db::schema::cases::columns::{id, user_id, guild_id};
        diesel::delete(cases)
            .filter(id.eq(&c_id))
            .filter(user_id.eq(&u_id))
            .filter(guild_id.eq(&g_id))
            .get_result(self.conn().deref())
    }
    /// Select a case
    /// Returns the case on success
    pub fn get_case(&self, c_id: i32, u_id: i64, g_id: i64) -> QueryResult<Case<Utc>> {
		self._get_case()
	}

	fn _get_case(&self, c_id: i32, u_id: i64, g_id: i64) -> QueryResult<Case<Utc>> {
        cases::table.find((c_id, u_id, g_id))
            .first(self.conn().deref())
    }*/
    /// Select all cases for a user
    /// Returns a vector of cases on success
    pub fn get_cases<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<Vec<Case<Utc>>>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._get_cases(user_id.into().into(), guild_id.into().into())
            .map(|v| v.into_iter()
                .map(|c| c.into())
            .collect())
	}

	fn _get_cases(&self, u_id: i64, g_id: i64) -> QueryResult<Vec<InternalCase<Utc>>> {
        use crate::db::schema::cases::columns::{guild_id, user_id};
        cases::table.filter(user_id.eq(&u_id))
            .filter(guild_id.eq(&g_id))
            .get_results(self.conn().deref())
    }
    /// Get the count of cases in the database
    pub fn count_cases(&self) -> QueryResult<i64> {
		self._count_cases()
	}

	fn _count_cases(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        cases::table.select(count_star())
            .get_result(self.conn().deref())
    }

    // Tag Tools
    /// Add a Tag
    /// Returns the Tag on success
    pub fn new_tag<G, S, U>(&self, author: U, guild_id: G, name: S, data: S) -> QueryResult<Tag>
        where G: Into<InternalGuildId>,
            S: Into<String>,
            U: Into<InternalUserId> {
		self._new_tag(author.into().into(), guild_id.into().into(), name.into(), data.into())
            .map(|t| t.into())
	}

	fn _new_tag(&self, author: i64, guild_id: i64, name: String, data: String) -> QueryResult<InternalTag> {
        let tag = NewTag {
            author,
            guild_id,
            name,
            data,
        };
        diesel::insert_into(tags::table)
            .values(&tag)
            .get_result(self.conn().deref())
    }
    /// Delete a Tag
    /// Returns the Tag on success.
    pub fn del_tag<G, S>(&self, guild_id: G, name: S) -> QueryResult<Tag>   
        where G: Into<InternalGuildId>,
            S: Into<String> {
		self._del_tag(guild_id.into().into(), name.into())
            .map(|t| t.into())
	}

	fn _del_tag(&self, g_id: i64, nm: String) -> QueryResult<InternalTag> {
        use crate::db::schema::tags::columns::{name, guild_id};
        diesel::delete(tags::table)
            .filter(name.eq(&nm))
            .filter(guild_id.eq(&g_id))
            .get_result(self.conn().deref())
    }
    /// Select a Tag
    /// Returns the Tag on success
    pub fn get_tag<G, S>(&self, guild_id: G, name: S) -> QueryResult<Tag>
        where G: Into<InternalGuildId>,
            S: Into<String> {
		self._get_tag(guild_id.into().into(), name.into())
            .map(|t| t.into())
	}

	fn _get_tag(&self, g_id: i64, nm: String) -> QueryResult<InternalTag> {
        tags::table.find((g_id, nm))
            .first(self.conn().deref())
    }
    /// Select all tags by guild
    /// Returns Vec<Tag> on success on success
    pub fn get_tags<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<Vec<Tag>> {
		self._get_tags(guild_id.into().into())
            .map(|v| v.into_iter()
                .map(|t| t.into())
            .collect())
	}

	fn _get_tags(&self, g_id: i64) -> QueryResult<Vec<InternalTag>> {
        use crate::db::schema::tags::columns::guild_id;
        tags::table.filter(guild_id.eq(&g_id))
            .get_results(self.conn().deref())
    }
    /// Update a tag
    /// Returns the new tag on success
    pub fn update_tag<G, S>(&self, guild_id: G, name: S, tag: Tag) -> QueryResult<Tag>
        where G: Into<InternalGuildId>,
            S: Into<String> {
		self._update_tag(guild_id.into().into(), name.into(), tag.into())
            .map(|t| t.into())
	}

	fn _update_tag(&self, g_id: i64, nm: String, tag: InternalTag) -> QueryResult<InternalTag> {
        let target = tags::table.find((g_id, nm));
        diesel::update(target)
            .set(&tag)
            .get_result(self.conn().deref())
    }
    /// Get the count of tags in the database
    pub fn count_tags(&self) -> QueryResult<i64> {
		self._count_tags()
	}

	fn _count_tags(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        tags::table.select(count_star())
            .get_result(self.conn().deref())
    }

    // Tag Tools
    /// Add a Hackban
    /// Returns the Hackban on success
    pub fn new_hackban<G, U>(&self, user_id: U, guild_id: G, reason: Option<String>) -> QueryResult<Hackban>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._new_hackban(user_id.into().into(), guild_id.into().into(), reason)
            .map(|h| h.into())
	}

	fn _new_hackban(&self, id: i64, guild_id: i64, reason: Option<String>) -> QueryResult<InternalHackban> {
        let hb = InternalHackban {
            id,
            guild_id,
            reason,
        };
        diesel::insert_into(hackbans::table)
            .values(&hb)
            .get_result(self.conn().deref())
    }
    /// Delete a Hackban
    /// Returns the Hackban on success.
    pub fn del_hackban<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<Hackban>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._del_hackban(user_id.into().into(), guild_id.into().into())
            .map(|h| h.into())
	}

	fn _del_hackban(&self, h_id: i64, g_id: i64) -> QueryResult<InternalHackban> {
        use crate::db::schema::hackbans::columns::{id, guild_id};
        diesel::delete(hackbans::table)
            .filter(id.eq(&h_id))
            .filter(guild_id.eq(&g_id))
            .get_result(self.conn().deref())
    }
    /// Select a Hackban
    /// Returns the Hackban on success
    pub fn get_hackban<G, U>(&self, user_id: U, guild_id: G) -> QueryResult<Hackban>
        where G: Into<InternalGuildId>,
            U: Into<InternalUserId> {
		self._get_hackban(user_id.into().into(), guild_id.into().into())
            .map(|h| h.into())
	}

	fn _get_hackban(&self, id: i64, g_id: i64) -> QueryResult<InternalHackban> {
        hackbans::table.find((id, g_id))
            .first(self.conn().deref())
    }
    /// Select all hackbans by guild
    /// Returns Vec<Hackban> on success on success
    pub fn get_hackbans<G: Into<InternalGuildId>>(&self, guild_id: G) -> QueryResult<Vec<Hackban>> {
		self._get_hackbans(guild_id.into().into())
            .map(|v| v.into_iter()
                .map(|h| h.into())
            .collect())
	}

	fn _get_hackbans(&self, g_id: i64) -> QueryResult<Vec<InternalHackban>> {
        use crate::db::schema::hackbans::columns::guild_id;
        hackbans::table.filter(guild_id.eq(&g_id))
            .get_results(self.conn().deref())
    }
    /// Get the count of hackbans in the database
    pub fn count_hackbans(&self) -> QueryResult<i64> {
		self._count_hackbans()
	}

	fn _count_hackbans(&self) -> QueryResult<i64> {
        use diesel::dsl::count_star;
        hackbans::table.select(count_star())
            .get_result(self.conn().deref())
    }
}