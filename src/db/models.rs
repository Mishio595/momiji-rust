use chrono::{DateTime, TimeZone};
use twilight_model::id::{ChannelId, GuildId, RoleId, UserId};
use super::schema::*;

#[derive(Debug)]
pub struct Guild {
    pub id: GuildId,
    pub admin_roles: Vec<RoleId>,
    pub audit: bool,
    pub audit_channel: ChannelId,
    pub audit_threshold: i16,
    pub autorole: bool,
    pub autoroles: Vec<RoleId>,
    pub ignored_channels: Vec<ChannelId>,
    pub ignore_level: i16,
    pub introduction: bool,
    pub introduction_channel: ChannelId,
    pub introduction_message: String,
    pub introduction_type: String,
    pub mod_roles: Vec<RoleId>,
    pub modlog: bool,
    pub modlog_channel: ChannelId,
    pub mute_setup: bool,
    pub prefix: String,
    pub welcome: bool,
    pub welcome_channel: ChannelId,
    pub welcome_message: String,
    pub welcome_type: String,
    pub commands: Vec<String>,
    pub logging: Vec<String>,
    pub register_member_role: Option<RoleId>,
    pub register_cooldown_role: Option<RoleId>,
    pub register_cooldown_duration: Option<i32>,
    pub cooldown_restricted_roles: Vec<RoleId>,
}

// Deprecated fields: nickname, roles
#[derive(Debug)]
pub struct User<Tz: TimeZone> {
    pub id: UserId,
    pub guild_id: GuildId,
    pub username: String,
    pub nickname: String,
    pub roles: Vec<RoleId>,
    pub watchlist: bool,
    pub xp: i64,
    pub last_message: DateTime<Tz>,
    pub registered: Option<DateTime<Tz>>,
}

#[derive(Debug)]
pub struct Note<Tz: TimeZone> {
    pub id: i32,
    pub user_id: UserId,
    pub guild_id: GuildId,
    pub note: String,
    pub moderator: UserId,
    pub timestamp: DateTime<Tz>,
}

#[derive(Debug)]
pub struct Role {
    pub id: RoleId,
    pub guild_id: GuildId,
    pub category: String,
    pub aliases: Vec<String>,
    pub required_roles: Vec<RoleId>,
    pub forbidden_roles: Vec<RoleId>,
}

#[derive(Debug)]
pub struct Timer {
    pub id: i32,
    pub starttime: i64,
    pub endtime: i64,
    pub data: String,
}

#[derive(Debug)]
pub struct Case<Tz: TimeZone> {
    pub id: i32,
    pub user_id: UserId,
    pub guild_id: GuildId,
    pub casetype: String,
    pub reason: String,
    pub moderator: UserId,
    pub timestamp: DateTime<Tz>,
}

#[derive(Debug)]
pub struct Tag {
    pub author: i64,
    pub guild_id: GuildId,
    pub name: String,
    pub data: String,
}

#[derive(Debug)]
pub struct Hackban {
    pub id: UserId,
    pub guild_id: GuildId,
    pub reason: Option<String>,
}

pub struct InternalChannelId(pub i64);
pub struct InternalGuildId(pub i64);
pub struct InternalRoleId(pub i64);
pub struct InternalUserId(pub i64);

// QUERYABLES

#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(id)]
#[table_name="guilds"]
pub(crate) struct InternalGuild {
    pub(crate) id: i64,
    pub(crate) admin_roles: Vec<i64>,
    pub(crate) audit: bool,
    pub(crate) audit_channel: i64,
    pub(crate) audit_threshold: i16,
    pub(crate) autorole: bool,
    pub(crate) autoroles: Vec<i64>,
    pub(crate) ignored_channels: Vec<i64>,
    pub(crate) ignore_level: i16,
    pub(crate) introduction: bool,
    pub(crate) introduction_channel: i64,
    pub(crate) introduction_message: String,
    pub(crate) introduction_type: String,
    pub(crate) mod_roles: Vec<i64>,
    pub(crate) modlog: bool,
    pub(crate) modlog_channel: i64,
    pub(crate) mute_setup: bool,
    pub(crate) prefix: String,
    pub(crate) welcome: bool,
    pub(crate) welcome_channel: i64,
    pub(crate) welcome_message: String,
    pub(crate) welcome_type: String,
    pub(crate) commands: Vec<String>,
    pub(crate) logging: Vec<String>,
    pub(crate) register_member_role: Option<i64>,
    pub(crate) register_cooldown_role: Option<i64>,
    pub(crate) register_cooldown_duration: Option<i32>,
    pub(crate) cooldown_restricted_roles: Vec<i64>,
}

// Deprecated fields: nickname, roles
#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(id, guild_id)]
#[table_name="users"]
pub(crate) struct InternalUser<Tz: TimeZone> {
    pub(crate) id: i64,
    pub(crate) guild_id: i64,
    pub(crate) username: String,
    pub(crate) nickname: String,
    pub(crate) roles: Vec<i64>,
    pub(crate) watchlist: bool,
    pub(crate) xp: i64,
    pub(crate) last_message: DateTime<Tz>,
    pub(crate) registered: Option<DateTime<Tz>>,
}

#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(id, user_id, guild_id)]
#[table_name="notes"]
pub(crate) struct InternalNote<Tz: TimeZone> {
    pub(crate) id: i32,
    pub(crate) user_id: i64,
    pub(crate) guild_id: i64,
    pub(crate) note: String,
    pub(crate) moderator: i64,
    pub(crate) timestamp: DateTime<Tz>,
}

#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(id, guild_id)]
#[table_name="roles"]
pub(crate) struct InternalRole {
    pub(crate) id: i64,
    pub(crate) guild_id: i64,
    pub(crate) category: String,
    pub(crate) aliases: Vec<String>,
    pub(crate) required_roles: Vec<i64>,
    pub(crate) forbidden_roles: Vec<i64>,
}

#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(id)]
#[table_name="timers"]
pub(crate) struct InternalTimer {
    pub(crate) id: i32,
    pub(crate) starttime: i64,
    pub(crate) endtime: i64,
    pub(crate) data: String,
}

#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(id, user_id, guild_id)]
#[table_name="cases"]
pub(crate) struct InternalCase<Tz: TimeZone> {
    pub(crate) id: i32,
    pub(crate) user_id: i64,
    pub(crate) guild_id: i64,
    pub(crate) casetype: String,
    pub(crate) reason: String,
    pub(crate) moderator: i64,
    pub(crate) timestamp: DateTime<Tz>
}

#[derive(Queryable, Identifiable, AsChangeset, Debug)]
#[primary_key(guild_id, name)]
#[table_name="tags"]
pub(crate) struct InternalTag {
    pub(crate) author: i64,
    pub(crate) guild_id: i64,
    pub(crate) name: String,
    pub(crate) data: String,
}

// This one would be the same for insertable or queryable, so it has both
#[derive(Queryable, Identifiable, AsChangeset, Insertable, Clone, Debug)]
#[primary_key(id, guild_id)]
#[table_name="hackbans"]
pub(crate) struct InternalHackban {
    pub(crate) id: i64,
    pub(crate) guild_id: i64,
    pub(crate) reason: Option<String>,
}

// END QUERYABLES
// INSERTABLES

#[derive(Insertable)]
#[table_name="guilds"]
pub struct NewGuild {
    pub id: i64,
}

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser {
    pub id: i64,
    pub guild_id: i64,
}

#[derive(Insertable)]
#[table_name="notes"]
pub struct NewNote {
    pub user_id: i64,
    pub guild_id: i64,
    pub note: String,
    pub moderator: i64,
}

#[derive(Insertable)]
#[table_name="roles"]
pub struct NewRole {
    pub id: i64,
    pub guild_id: i64,
    pub category: Option<String>,
    pub aliases: Option<Vec<String>>,
}

#[derive(Insertable)]
#[table_name="timers"]
pub struct NewTimer {
    pub starttime: i64,
    pub endtime: i64,
    pub data: String,
}

#[derive(Insertable)]
#[table_name="cases"]
pub struct NewCase {
    pub user_id: i64,
    pub guild_id: i64,
    pub casetype: String,
    pub reason: Option<String>,
    pub moderator: i64,
}

#[derive(Insertable)]
#[table_name="tags"]
pub struct NewTag {
    pub author: i64,
    pub guild_id: i64,
    pub name: String,
    pub data: String,
}

// END INSERTABLES
// OTHER STUFF

#[derive(Insertable, AsChangeset, Debug)]
#[table_name="users"]
#[primary_key(id, guild_id)]
pub struct UserUpdate {
    pub id: i64,
    pub guild_id: i64,
    pub username: String,
}