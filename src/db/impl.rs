use super::{
    models::{self, InternalChannelId, InternalGuildId, InternalRoleId, InternalUserId}
};
use chrono::{TimeZone, Utc};
use twilight_model::id::{ChannelId, GuildId, RoleId, UserId};
use std::fmt::{Display, Formatter, Result as FmtResult};

impl From<ChannelId> for InternalChannelId {
    fn from(id: ChannelId) -> Self {
        Self(id.0 as i64)
    }
}

impl From<GuildId> for InternalGuildId {
    fn from(id: GuildId) -> Self {
        Self(id.0 as i64)
    }
}

impl From<RoleId> for InternalRoleId {
    fn from(id: RoleId) -> Self {
        Self(id.0 as i64)
    }
}

impl From<UserId> for InternalUserId {
    fn from(id: UserId) -> Self {
        Self(id.0 as i64)
    }
}

impl From<InternalChannelId> for ChannelId {
    fn from(id: InternalChannelId) -> Self {
        Self(id.0 as u64)
    }
}

impl From<InternalGuildId> for GuildId {
    fn from(id: InternalGuildId) -> Self {
        Self(id.0 as u64)
    }
}

impl From<InternalRoleId> for RoleId {
    fn from(id: InternalRoleId) -> Self {
        Self(id.0 as u64)
    }
}

impl From<InternalUserId> for UserId {
    fn from(id: InternalUserId) -> Self {
        Self(id.0 as u64)
    }
}

impl From<i64> for InternalChannelId {
    fn from(id: i64) -> Self {
        Self(id)
    }
}

impl From<i64> for InternalGuildId {
    fn from(id: i64) -> Self {
        Self(id)
    }
}

impl From<i64> for InternalRoleId {
    fn from(id: i64) -> Self {
        Self(id)
    }
}

impl From<i64> for InternalUserId {
    fn from(id: i64) -> Self {
        Self(id)
    }
}

impl From<InternalChannelId> for i64 {
    fn from(id: InternalChannelId) -> Self {
        id.0
    }
}

impl From<InternalGuildId> for i64 {
    fn from(id: InternalGuildId) -> Self {
        id.0
    }
}

impl From<InternalRoleId> for i64 {
    fn from(id: InternalRoleId) -> Self {
        id.0
    }
}

impl From<InternalUserId> for i64 {
    fn from(id: InternalUserId) -> Self {
        id.0
    }
}

impl From<models::InternalGuild> for models::Guild {
    fn from(guild: models::InternalGuild) -> Self {
        let admin_roles = guild.admin_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let autoroles = guild.autoroles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let ignored_channels = guild.ignored_channels.into_iter()
            .map(|id| InternalChannelId::from(id).into())
            .collect();
        let mod_roles = guild.mod_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let cooldown_restricted_roles = guild.cooldown_restricted_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let register_member_role = guild.register_member_role
            .map(|id| InternalRoleId::from(id).into());
        let register_cooldown_role = guild.register_cooldown_role
            .map(|id| InternalRoleId::from(id).into());

        Self {
            id: InternalGuildId::from(guild.id).into(),
            admin_roles,
            audit: guild.audit,
            audit_channel: InternalChannelId::from(guild.audit_channel).into(),
            audit_threshold: guild.audit_threshold,
            autorole: guild.autorole,
            autoroles,
            ignored_channels,
            ignore_level: guild.ignore_level,
            introduction: guild.introduction,
            introduction_channel: InternalChannelId::from(guild.introduction_channel).into(),
            introduction_message: guild.introduction_message,
            introduction_type: guild.introduction_type,
            mod_roles,
            modlog: guild.modlog,
            modlog_channel: InternalChannelId::from(guild.modlog_channel).into(),
            mute_setup: guild.mute_setup,
            prefix: guild.prefix,
            welcome: guild.welcome,
            welcome_channel: InternalChannelId::from(guild.welcome_channel).into(),
            welcome_message: guild.welcome_message,
            welcome_type: guild.welcome_type,
            commands: guild.commands,
            logging: guild.logging,
            register_member_role,
            register_cooldown_role,
            register_cooldown_duration: guild.register_cooldown_duration,
            cooldown_restricted_roles,
        }
    }
}

impl<Tz: TimeZone> From<models::InternalUser<Tz>> for models::User<Tz> {
    fn from(user: models::InternalUser<Tz>) -> Self {
        let roles = user.roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();

        Self {
            id: InternalUserId::from(user.id).into(),
            guild_id: InternalGuildId::from(user.guild_id).into(),
            username: user.username,
            nickname: user.nickname,
            roles,
            watchlist: user.watchlist,
            xp: user.xp,
            last_message: user.last_message,
            registered: user.registered,
        }
    }
}

impl<Tz: TimeZone> From<models::InternalNote<Tz>> for models::Note<Tz> {
    fn from(note: models::InternalNote<Tz>) -> Self {
        Self {
            id: note.id,
            user_id: InternalUserId::from(note.user_id).into(),
            guild_id: InternalGuildId::from(note.guild_id).into(),
            note: note.note,
            moderator: InternalUserId::from(note.moderator).into(),
            timestamp: note.timestamp,
        }
    }
}

impl From<models::InternalRole> for models::Role {
    fn from(role: models::InternalRole) -> Self {
        let required_roles = role.required_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let forbidden_roles = role.forbidden_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();

        Self {
            id: InternalRoleId::from(role.id).into(),
            guild_id: InternalGuildId::from(role.guild_id).into(),
            category: role.category,
            aliases: role.aliases,
            required_roles,
            forbidden_roles,
        }
    }
}

impl From<models::InternalTimer> for models::Timer {
    fn from(timer: models::InternalTimer) -> Self {
        Self {
            id: timer.id,
            starttime: timer.starttime,
            endtime: timer.endtime,
            data: timer.data,
        }
    }
}

impl<Tz: TimeZone> From<models::InternalCase<Tz>> for models::Case<Tz> {
    fn from(case: models::InternalCase<Tz>) -> Self {
        Self {
            id: case.id,
            user_id: InternalUserId::from(case.user_id).into(),
            guild_id: InternalGuildId::from(case.guild_id).into(),
            casetype: case.casetype,
            reason: case.reason,
            moderator: InternalUserId::from(case.moderator).into(),
            timestamp: case.timestamp,
        }
    }
}

impl From<models::InternalTag> for models::Tag {
    fn from(tag: models::InternalTag) -> Self {
        Self {
            author: tag.author,
            guild_id: InternalGuildId::from(tag.guild_id).into(),
            name: tag.name,
            data: tag.data,
        }
    }
}

impl From<models::InternalHackban> for models::Hackban {
    fn from(hackban: models::InternalHackban) -> Self {
        Self {
            id: InternalUserId::from(hackban.id).into(),
            guild_id: InternalGuildId::from(hackban.guild_id).into(),
            reason: hackban.reason,
        }
    }
}

impl From<models::Guild> for models::InternalGuild {
    fn from(guild: models::Guild) -> Self {
        let admin_roles = guild.admin_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let autoroles = guild.autoroles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let ignored_channels = guild.ignored_channels.into_iter()
            .map(|id| InternalChannelId::from(id).into())
            .collect();
        let mod_roles = guild.mod_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let cooldown_restricted_roles = guild.cooldown_restricted_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let register_member_role = guild.register_member_role
            .map(|id| InternalRoleId::from(id).into());
        let register_cooldown_role = guild.register_cooldown_role
            .map(|id| InternalRoleId::from(id).into());

        Self {
            id: InternalGuildId::from(guild.id).into(),
            admin_roles,
            audit: guild.audit,
            audit_channel: InternalChannelId::from(guild.audit_channel).into(),
            audit_threshold: guild.audit_threshold,
            autorole: guild.autorole,
            autoroles,
            ignored_channels,
            ignore_level: guild.ignore_level,
            introduction: guild.introduction,
            introduction_channel: InternalChannelId::from(guild.introduction_channel).into(),
            introduction_message: guild.introduction_message,
            introduction_type: guild.introduction_type,
            mod_roles,
            modlog: guild.modlog,
            modlog_channel: InternalChannelId::from(guild.modlog_channel).into(),
            mute_setup: guild.mute_setup,
            prefix: guild.prefix,
            welcome: guild.welcome,
            welcome_channel: InternalChannelId::from(guild.welcome_channel).into(),
            welcome_message: guild.welcome_message,
            welcome_type: guild.welcome_type,
            commands: guild.commands,
            logging: guild.logging,
            register_member_role,
            register_cooldown_role,
            register_cooldown_duration: guild.register_cooldown_duration,
            cooldown_restricted_roles,
        }
    }
}

impl<Tz: TimeZone> From<models::User<Tz>> for models::InternalUser<Tz> {
    fn from(user: models::User<Tz>) -> Self {
        let roles = user.roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();

        Self {
            id: InternalUserId::from(user.id).into(),
            guild_id: InternalGuildId::from(user.guild_id).into(),
            username: user.username,
            nickname: user.nickname,
            roles,
            watchlist: user.watchlist,
            xp: user.xp,
            last_message: user.last_message,
            registered: user.registered,
        }
    }
}

impl<Tz: TimeZone> From<models::Note<Tz>> for models::InternalNote<Tz> {
    fn from(note: models::Note<Tz>) -> Self {
        Self {
            id: note.id,
            user_id: InternalUserId::from(note.user_id).into(),
            guild_id: InternalGuildId::from(note.guild_id).into(),
            note: note.note,
            moderator: InternalUserId::from(note.moderator).into(),
            timestamp: note.timestamp,
        }
    }
}

impl From<models::Role> for models::InternalRole {
    fn from(role: models::Role) -> Self {
        let required_roles = role.required_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();
        let forbidden_roles = role.forbidden_roles.into_iter()
            .map(|id| InternalRoleId::from(id).into())
            .collect();

        Self {
            id: InternalRoleId::from(role.id).into(),
            guild_id: InternalGuildId::from(role.guild_id).into(),
            category: role.category,
            aliases: role.aliases,
            required_roles,
            forbidden_roles,
        }
    }
}

impl From<models::Timer> for models::InternalTimer {
    fn from(timer: models::Timer) -> Self {
        Self {
            id: timer.id,
            starttime: timer.starttime,
            endtime: timer.endtime,
            data: timer.data,
        }
    }
}

impl<Tz: TimeZone> From<models::Case<Tz>> for models::InternalCase<Tz> {
    fn from(case: models::Case<Tz>) -> Self {
        Self {
            id: case.id,
            user_id: InternalUserId::from(case.user_id).into(),
            guild_id: InternalGuildId::from(case.guild_id).into(),
            casetype: case.casetype,
            reason: case.reason,
            moderator: InternalUserId::from(case.moderator).into(),
            timestamp: case.timestamp,
        }
    }
}

impl From<models::Tag> for models::InternalTag {
    fn from(tag: models::Tag) -> Self {
        Self {
            author: tag.author,
            guild_id: InternalGuildId::from(tag.guild_id).into(),
            name: tag.name,
            data: tag.data,
        }
    }
}

impl From<models::Hackban> for models::InternalHackban {
    fn from(hackban: models::Hackban) -> Self {
        Self {
            id: InternalUserId::from(hackban.id).into(),
            guild_id: InternalGuildId::from(hackban.guild_id).into(),
            reason: hackban.reason,
        }
    }
}

impl Display for models::Guild {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "**Admin Roles:** {}\n**Audit:** {}\n**Audit Channel:** {}\n**Audit Threshold:** {}\n**Autorole:** {}\n**Autoroles:** {}\n**Ignored Channels:** {}\n**Ignore Level:** {}\n**Introduction:** {}\n**Introduction Channel:** {}\n**Introduction Type:** {}\n**Introduction Message:** {}\n**Mod Roles: ** {}\n**Modlog:** {}\n**Modlog Channel:** {}\n**Mute Setup:** {}\n**Prefix:** {}\n**Welcome:** {}\n**Welcome Channel:** {}\n**Welcome Type:** {}\n**Welcome Message:** {}\n**Disabled Commands:** {}\n**Disabled Log Types:** {}\n**Register Member Role:** {}\n**Register Cooldown Role:** {}\n**Register Duration:** {}\n**Cooldown Restricted Roles:** {}",
            self.admin_roles.iter().map(|e| e.to_string()).collect::<Vec<String>>().join(", "),
            self.audit,
            format!("<#{}>", self.audit_channel),
            self.audit_threshold,
            self.autorole,
            self.autoroles.iter().map(|e| e.to_string()).collect::<Vec<String>>().join(", "),
            self.ignored_channels.iter().map(|e| format!("<#{}>", e)).collect::<Vec<String>>().join(", "),
            self.ignore_level,
            self.introduction,
            format!("<#{}>", self.introduction_channel),
            self.introduction_type,
            self.introduction_message,
            self.mod_roles.iter().map(|e| e.to_string()).collect::<Vec<String>>().join(", "),
            self.modlog,
            format!("<#{}>", self.modlog_channel),
            self.mute_setup,
            self.prefix,
            self.welcome,
            format!("<#{}>", self.welcome_channel),
            self.welcome_type,
            self.welcome_message,
            self.commands.join(", "),
            self.logging.join(", "),
            self.register_member_role.map(|e| e.to_string()).unwrap_or_else(|| "Not set".to_string()),
            self.register_cooldown_role.map(|e| e.to_string()).unwrap_or_else(|| "Not set".to_string()),
            self.register_cooldown_duration.map(|e| crate::core::utils::seconds_to_hrtime(e as usize)).unwrap_or_else(|| "Not set".to_string()),
            self.cooldown_restricted_roles.iter().map(|e| e.to_string()).collect::<Vec<String>>().join(", ")
    )}
}

impl Display for models::Note<Utc> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{} wrote on {} (ID: {})\n`{}`",
            self.moderator,
            self.timestamp.format("%a, %d %h %Y @ %H:%M:%S").to_string(),
            self.id,
            self.note)
    }
}