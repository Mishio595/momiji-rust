use crate::commands;
use momiji::framework::{Config, Framework};

pub fn standard_framework() -> Framework {
    let config = Config::builder()
        .prefix("m!")
        .dynamic_prefix(|message, ctx| {
            match message.guild_id {
                None => Some(String::new()),
                Some(guild_id) => {
                    if let Ok(settings) = ctx.db.get_guild(guild_id) {
                        Some(settings.prefix)
                    } else {
                        None
                    }
                }
            }
        })
        .build();

    Framework::builder()
        .config(config)
        .add_module("Config", commands::init_config)
        .add_module("Management", commands::init_management)
        .add_module("Miscellaneous", commands::init_misc)
        .add_module("Self Roles", commands::init_self_roles)
        .add_module("Self Role Management", commands::init_self_role_management)
        .add_module("Tags", commands::init_tags)
        .add_module("Database Controls", commands::init_db_controls)
        .add_module("Owner Tools", commands::init_owner_commands)
        .add_module("Role Control", commands::init_role_control)
        .build()
}