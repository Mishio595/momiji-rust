use momiji::{
    Context,
    core::utils::{
        hrtime_to_seconds,
        seconds_to_hrtime,
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct RegisterDuration;
#[async_trait]
impl Command for RegisterDuration {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Set the duration cooldown is applied for. Default value is 24 hours.")
            .usage("<time_resolvable>")
            .example("24h")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut settings = ctx.db.get_guild(guild_id)?;
            if let Ok(dur) = args.rest().parse::<String>() {
                let dur = hrtime_to_seconds(dur);
                settings.register_cooldown_duration = Some(dur as i32);
                ctx.db.update_guild(guild_id, settings)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Set duration of cooldown to {}", seconds_to_hrtime(dur as usize)))?.await?;
            }
        }
        
        Ok(())
    }
}