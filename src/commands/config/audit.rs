use momiji::{
    Context,
    core::{
        consts::colors,
        utils::parse_channel,
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigAudit;
#[async_trait]
impl Command for ConfigAudit {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Change audit log settings. A channel must be provided for channel.")
            .usage("<enable|disable|channel> <channel_resolvable>")
            .example("channel #audit-logs")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut guild_data = ctx.db.get_guild(guild_id)?;
            let op = args.single::<String>().unwrap_or_default();
            let mut val = args.rest().to_string();
            match op.to_lowercase().as_str() {
                "enable" => {
                    guild_data.audit = true;
                },
                "disable" => {
                    guild_data.audit = false;
                },
                "channel" => {
                    match parse_channel(val.to_string(), guild_id, ctx.clone()) {
                        Some((channel_id, channel)) => {
                            guild_data.audit_channel = channel_id;
                            val = format!("{} ({})", channel.name(), channel_id.0);
                        },
                        None => {
                            ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that channel.")?.await?;
                            return Ok(())
                        },
                    }
                },
                "threshold" => {
                    match val.parse::<i16>() {
                        Ok(th) => {
                            guild_data.audit_threshold = th;
                            val = th.to_string();
                        },
                        Err(_) => { ctx.http.create_message(message.channel_id).reply(message.id).content("Please input a number as the threshold")?.await?; }
                    }
                },
                _ => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("I didn't understand that option. Valid options are: `enable`, `disable`, `channel`, `threshold`. For more information see `help config audit`")?.await?;
                    return Ok(())
                },
            }
            let guild = ctx.db.update_guild(guild_id, guild_data)?;
            
            let embed = EmbedBuilder::new()
                .title("Config Audit Summary")
                .color(colors::MAIN)
                .description(format!("**Operation:** {}\n**Value:** {}",
                    op,
                    if val.is_empty() { format!("{}", guild.audit) } else { val },
                ))
                .build()?;
            
            ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
        }
        
        Ok(())
    }
}