use momiji::{
    Context,
    core::{
        consts::colors,
        utils::parse_channel,
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigModlog;
#[async_trait]
impl Command for ConfigModlog {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Change moderation log settings. A channel must be provided for channel.")
            .usage("<enable|disable|channel> <channel_resolvable>")
            .example("channel #mod-logs")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut guild_data = ctx.db.get_guild(guild_id)?;
            let op = args.single::<String>().unwrap_or_default();
            let mut val = args.rest().to_string();
            match op.to_lowercase().as_str() {
                "enable" => {
                    guild_data.modlog = true;
                },
                "disable" => {
                    guild_data.modlog = false;
                },
                "channel" => {
                    match parse_channel(val.to_string(), guild_id, ctx.clone()) {
                        Some((channel_id, channel)) => {
                            guild_data.modlog_channel = channel_id;
                            val = format!("{} ({})", channel.name(), channel_id.0);
                        },
                        None => {
                            ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that channel.")?.await?;
                            return Ok(())
                        },
                    }
                },
                _ => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("I didn't understand that option. Valid options are: `enable`, `disable`, `channel`. For more information see `help config modlog`")?.await?;
                    return Ok(())
                },
            }
            let guild = ctx.db.update_guild(guild_id, guild_data)?;
            
            let embed = EmbedBuilder::new()
                .title("Config Modlog Summary")
                .color(colors::MAIN)
                .description(format!("**Operation:** {}\n**Value:** {}",
                    op,
                    if val.is_empty() { guild.modlog.to_string() } else { val },
                ))
                .build()?;

            ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
        }
        
        Ok(())
    }
}