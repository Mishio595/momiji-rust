use momiji::{
    Context,
    core::consts::{
        colors,
        LOG_TYPES,
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigLogs;
#[async_trait]
impl Command for ConfigLogs {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Change which log messages are disabled. A log type must be provided.")
            .usage("<enable|disable|types> [type]")
            .example("disable message_edit")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut guild_data = ctx.db.get_guild(guild_id)?;
            let op = args.single::<String>().unwrap_or_default();
            let val = args.rest().to_string();
            match op.to_lowercase().as_str() {
                "enable" => {
                    guild_data.logging.retain(|e| *e != val);
                },
                "disable" => {
                    if LOG_TYPES.contains(&val.as_str()) {
                        guild_data.logging.push(val.clone());
                    } else {
                        ctx.http.create_message(message.channel_id).reply(message.id).content("Invalid log type. See `config log types` for valid types.")?.await?;
                        return Ok(());
                    }
                },
                "types" => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content(LOG_TYPES.iter()
                        .map(|e| format!("`{}`", e))
                        .collect::<Vec<String>>()
                        .join(", "))?
                        .await?;
                    return Ok(());
                },
                _ => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("I didn't understand that option. Valid options are: `enable`, `disable`. For more information see `help config log`")?.await?;
                    return Ok(())
                },
            }
            ctx.db.update_guild(guild_id, guild_data)?;
            
            let embed = EmbedBuilder::new()
                .title("Config Log Summary")
                .color(colors::MAIN)
                .description(format!("**Operation:** {}\n**Value:** {}",
                    op,
                    val,
                ))
                .build()?;

            ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
        }
        
        Ok(())
    }
}