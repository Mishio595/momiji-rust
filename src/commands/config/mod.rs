pub mod audit;
pub mod autorole;
pub mod commands;
pub mod introduction;
pub mod list;
pub mod logs;
pub mod modlog;
pub mod prefix;
pub mod raw;
pub mod register_member;
pub mod register_cooldown;
pub mod register_duration;
pub mod register_roles;
pub mod welcome;

use momiji::framework::command::ModuleBuilder;
use std::sync::Arc;

pub fn init_config(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(true)
        .guild_only(true)
        .prefix("config")
        .default_command(Arc::new(list::ConfigList))
        .add_command("list", Arc::new(list::ConfigList))
        .add_command("raw", Arc::new(raw::ConfigRaw))
        .add_command("prefix", Arc::new(prefix::ConfigPrefix))
        .add_command("autorole", Arc::new(autorole::ConfigAutorole))
        .add_command("audit", Arc::new(audit::ConfigAudit))
        .add_command("modlog", Arc::new(modlog::ConfigModlog))
        .add_command("welcome", Arc::new(welcome::ConfigWelcome))
        .add_command("introduction", Arc::new(introduction::ConfigIntroduction))
        .add_alias("intro", "introduction")
        .add_command("cmd", Arc::new(commands::ConfigCommands))
        .add_command("log", Arc::new(logs::ConfigLogs))
        .add_command("register_member", Arc::new(register_member::RegisterMember))
        .add_command("register_cooldown", Arc::new(register_cooldown::RegisterCooldown))
        .add_command("register_duration", Arc::new(register_duration::RegisterDuration))
        .add_command("register_roles", Arc::new(register_roles::RegisterRestrictions))
        .add_alias("reg_member", "register_member")
        .add_alias("reg_cooldown", "register_cooldown")
        .add_alias("reg_duration", "register_duration")
        .add_alias("reg_roles", "register_roles")
}