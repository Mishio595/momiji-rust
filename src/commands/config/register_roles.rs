use momiji::{
    Context,
    core::utils::parse_role,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct RegisterRestrictions;
#[async_trait]
impl Command for RegisterRestrictions {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Manage the roles people on cooldown cannot self-assign. These are also ignored in register command usage. Valid options: `add`, `remove`, `set`")
            .usage("<option> [values]")
            .example("set selfies, nsfw")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let op = args.single::<String>().unwrap_or_default();
            let mut sec = "";
            let mut val = String::new();
            let mut settings = ctx.db.get_guild(guild_id)?;
            match op.as_str() {
                "add" => {
                    if let Some((role_id, role)) = parse_role(args.rest().to_string(), guild_id, ctx.clone()) {
                        settings.cooldown_restricted_roles.push(role_id);
                        sec = "Added";
                        val = role.name;
                    }
                },
                "remove" => {
                    if let Some((role_id, role)) = parse_role(args.rest().to_string(), guild_id, ctx.clone()) {
                        settings.cooldown_restricted_roles.push(role_id);
                        sec = "Removed";
                        val = role.name;
                    }
                },
                "set" => {
                    let list = args.rest().split(',').map(|s| s.trim().to_string());
                    let mut roles = Vec::new();
                    let mut role_names = Vec::new();
                    for role in list {
                        if let Some((role_id, role)) = parse_role(role, guild_id, ctx.clone()) {
                            roles.push(role_id);
                            role_names.push(role.name.clone());
                        }
                    }
                    settings.cooldown_restricted_roles = roles;
                    sec = "Set to";
                    val = role_names.join(", ");
                },
                _ => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("I didn't understand that option. Valid options are: `add`, `remove`, `set`. For more information see `help p reg_roles`")?.await?;
                    return Ok(())
                },
            }
            ctx.db.update_guild(guild_id, settings)?;
            ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Successfully modified restricted roles. {} {}", sec, val))?.await?;
        }

        Ok(())
    }
}