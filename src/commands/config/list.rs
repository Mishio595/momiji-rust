use momiji::{
    Context,
    core::consts::colors,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigList;
#[async_trait]
impl Command for ConfigList {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Lists current configuration.")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, _: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let guild_data = ctx.db.get_guild(guild_id)?;

            let embed = EmbedBuilder::new()
                .color(colors::MAIN)
                .description(format!("{}", guild_data))
                .build()?;

            ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
        }

        Ok(())
    }
}
