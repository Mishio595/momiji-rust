use momiji::{
    Context,
    core::{
        consts::colors,
        utils::parse_channel,
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigWelcome;
#[async_trait]
impl Command for ConfigWelcome {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Change welcome message settings.\nOption is one of enable, disable, channel, message, type and the respective values should be none, none, channel_resolvable, desired message.\nType designates if the message is plain or embed. Anything other than embed will result in plain.")
            .usage("<option> <value>")
            .example("message Welcome to {guild}, {user}!")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut guild_data = ctx.db.get_guild(guild_id)?;
            let op = args.single::<String>().unwrap_or_default();
            let mut val = args.rest().to_string();
            match op.to_lowercase().as_str() {
                "enable" => {
                    guild_data.welcome = true;
                },
                "disable" => {
                    guild_data.welcome = false;
                },
                "channel" => {
                    match parse_channel(val.to_string(), guild_id, ctx.clone()) {
                        Some((channel_id, channel)) => {
                            guild_data.welcome_channel = channel_id;
                            val = format!("{} ({})", channel.name(), channel_id.0);
                        },
                        None => {
                            ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that channel.")?.await?;
                            return Ok(())
                        },
                    }
                },
                "message" => {
                    guild_data.welcome_message = val.to_string();
                },
                "type" => {
                    guild_data.welcome_type = val.to_string();
                },
                _ => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("I didn't understand that option. Valid options are: `enable`, `disable`, `channel`, `message`, `type`. For more information see `help config welcome`")?.await?;
                    return Ok(())
                },
            }
            let guild = ctx.db.update_guild(guild_id, guild_data)?;
            
            let embed = EmbedBuilder::new()
                .title("Config Welcome Summary")
                .color(colors::MAIN)
                .description(format!("**Operation:** {}\n**Value:** {}",
                    op,
                    if val.is_empty() { guild.welcome.to_string() } else { val },
                ))
                .build()?;
            
            ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
        }
        
        Ok(())
    }
}