use momiji::{
    Context,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigRaw;
#[async_trait]
impl Command for ConfigRaw {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Lists current configuration as raw output.")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, _: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let guild_data = ctx.db.get_guild(guild_id)?;
            ctx.http.create_message(message.channel_id).reply(message.id).content(format!("{:?}", guild_data))?.await?;
        }

        Ok(())
    }
}