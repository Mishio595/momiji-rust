use momiji::{
    Context,
    core::{
        consts::colors,
        utils::parse_role,
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigAutorole;
#[async_trait]
impl Command for ConfigAutorole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Change autorole settings. A role must be provided for add or remove.")
            .usage("<add|remove|enable|disable> <role_resolvable|_>")
            .example("add member")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut guild_data = ctx.db.get_guild(guild_id)?;
            let op = args.single::<String>().unwrap_or_default();
            let mut val = args.rest().to_string();
            match op.to_lowercase().as_str() {
                "add" => {
                    match parse_role(val.to_string(), guild_id, ctx.clone()) {
                        Some((role_id, role)) => {
                            guild_data.autoroles.push(role_id);
                            val = format!("{} ({})", role.name, role_id.0);
                        },
                        None => {
                            ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that role.")?.await?;
                            return Ok(())
                        },
                    }
                },
                "remove" => {
                    match parse_role(val.to_string(), guild_id, ctx.clone()) {
                        Some((role_id, role)) => {
                            guild_data.autoroles.retain(|e| *e != role_id);
                            val = format!("{} ({})", role.name, role_id.0);
                        },
                        None => {
                            ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that role.")?.await?;
                            return Ok(())
                        },
                    }
                },
                "enable" => {
                    guild_data.autorole = true;
                },
                "disable" => {
                    guild_data.autorole = false;
                },
                _ => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("I didn't understand that option. Valid options are: `add`, `remove`, `enable`, `disable`. For more information see `help config autorole`")?.await?;
                    return Ok(())
                },
            }
            let guild = ctx.db.update_guild(guild_id, guild_data)?;

            let embed = EmbedBuilder::new()
                .title("Config Autorole Summary")
                .color(colors::MAIN)
                .description(format!("**Operation:** {}\n**Value:** {}",
                    op,
                    if val.is_empty() { guild.autorole.to_string() } else { val } ,
                ))
                .build()?;

            ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
        }
        
        Ok(())
    }
}