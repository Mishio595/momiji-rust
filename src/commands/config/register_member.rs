use momiji::{
    Context,
    core::utils::parse_role,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct RegisterMember;
#[async_trait]
impl Command for RegisterMember {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Set the member role used by register. This role is automatically either after cooldown, if cooldown is set, or right away.")
            .usage("<role_resolvable>")
            .example("member")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut settings = ctx.db.get_guild(guild_id)?;
            if let Some((role_id, role)) = parse_role(args.rest().to_string(), guild_id, ctx.clone()) {
                settings.register_member_role = Some(role_id);
                ctx.db.update_guild(guild_id, settings)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Set member role to {}", role.name))?.await?;
            }
        }
        
        Ok(())
    }
}