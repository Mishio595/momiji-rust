use momiji::{
    Context,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct ConfigPrefix;
#[async_trait]
impl Command for ConfigPrefix {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Set a new prefix.")
            .usage("<prefix>")
            .example("!!")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let mut guild_data = ctx.db.get_guild(guild_id)?;
            let pre = args.single::<String>()?;
            guild_data.prefix = pre;
            match ctx.db.update_guild(guild_id, guild_data) {
                Ok(guild) => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Set prefix to {}", guild.prefix))?.await?;
                },
                Err(_) => {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("Failed to change prefix")?.await?;
                },
            }
        }
        
        Ok(())
    }
}