use momiji::{
    Context,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::channel::Message;
use std::{error::Error, sync::Arc};

pub struct Prefix;
#[async_trait]
impl Command for Prefix {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Echoes the prefix of the current guild.")
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, _: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            if let Ok(settings) = ctx.db.get_guild(guild_id) {
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("The prefix for this guild is `{}`", settings.prefix))?.await?;
            } else {
                ctx.http.create_message(message.channel_id).reply(message.id).content("Failed to get guild data.")?.await?;
            }
        }
        Ok(())
    }
}