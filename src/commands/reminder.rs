use chrono::Utc;
use momiji::{
    Context,
    core::utils::{hrtime_to_seconds, get_switches, seconds_to_hrtime},
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::channel::Message;
use std::{error::Error, sync::Arc};

pub struct Reminder;
#[async_trait]
impl Command for Reminder {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Set a reminder. The reminder is sent to whatever channel it originated in.")
            .usage("<reminder text> </t time_resolvable>")
            .example("do the thing /t 1 day 10 min 25 s")
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let channel_id = message.channel_id;
        let user_id = message.author.id;
        let switches = get_switches(args.rest().to_string());
        let reminder = match switches.get("rest") {
            Some(s) => s.clone(),
            None => String::new(),
        };
        let start_time = Utc::now().timestamp();
        let dur = hrtime_to_seconds(match switches.get("t") {
            Some(s) => s.clone(),
            None => String::new(),
        });
        if dur>0 {
            let end_time = start_time + dur;
            let reminder_fmt = format!("REMINDER||{}||{}||{}||{}", channel_id.0, user_id.0, dur, reminder);
            ctx.db.new_timer(start_time, end_time, reminder_fmt.clone())?;
            ctx.tc.request();
            ctx.http.create_message(channel_id).content(format!("Got it! I'll remind you to {} in {}",
                reminder,
                seconds_to_hrtime(dur as usize)
            ))?.await?;
        } else {
            ctx.http.create_message(channel_id).content("Sorry, I wasn't able to find a time there. Make sure you to add `/t time_resolvable` after your reminder text.")?.await?;
        }

        Ok(())
    }
}