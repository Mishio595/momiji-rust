// use momiji::core::consts::*;
use momiji::Context;
use momiji::framework::args::Args;
use momiji::framework::command::{Command, Options};
use twilight_model::{channel::Message, id::GuildId};
use std::sync::Arc;
use std::error::Error;

pub struct NewGuild;
#[async_trait]
impl Command for NewGuild {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .owner_only(true)
            .help_available(false)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let id = args.single::<u64>()?;
        ctx.db.new_guild(GuildId(id))?;
        ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Created guild entry for {}", id))?.await?;

        Ok(())
    }
}