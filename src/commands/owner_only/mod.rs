pub mod log;
pub mod db;
pub mod cache;

use self::log::*;
use self::db::*;
use self::cache::*;
use momiji::framework::command::ModuleBuilder;
use std::sync::Arc;

pub fn init_owner_commands(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(false)
        .add_command("log", Arc::new(Log))
        .add_command("cache", Arc::new(CacheStats))
}

pub fn init_db_controls(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(false)
        .prefix("db")
        .add_command("new_guild", Arc::new(NewGuild))
}