use momiji::{
    Context,
    core::utils::parse_role,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct RoleColour;
#[async_trait]
impl Command for RoleColour {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Change the colour of a role.")
            .usage("<role_resolvable> <colour>")
            .example("418130449089691658 00ff00")
            .permissions(Permissions::MANAGE_ROLES)
            .guild_only(true)
            .build();

        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            match parse_role(args.single_quoted::<String>().unwrap_or_default(), guild_id, ctx.clone()) {
                Some((role_id, role)) => {
                    let input = args.single::<String>()?;
                    let color_as_hex = if let Some(stripped) = input.strip_prefix('#') {
                        stripped
                    } else {
                        input.as_str()
                    };
                    let color = u32::from_str_radix(color_as_hex, 16)?;
                    ctx.http.update_role(guild_id, role_id).color(color).await?;
                    ctx.http.create_message(message.channel_id).reply(message.id)
                        .content(format!("Colour of `{}` changed to `#{:06X}`", role.name, color))?
                        .await?;
                },
                None => { ctx.http.create_message(message.channel_id).reply(message.id)
                    .content("I couldn't find that role")?.await?;
                },
            }
        }
        Ok(())
    }
}