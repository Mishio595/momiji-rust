use momiji::{
    Context,
    core::{
        consts::colors,
        utils::{parse_role, parse_user},
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder};
use twilight_mention::Mention;
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};
use super::{filter_roles, get_highest_role};

pub struct RemoveRole;
#[async_trait]
impl Command for RemoveRole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Remove role(s) from a user.")
            .usage("<user_resolvable> <role_resolvables as CSV>")
            .example("@Adelyn red, green")
            .permissions(Permissions::MANAGE_ROLES)
            .guild_only(true)
            .build();

        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            if let Some((_, member)) = parse_user(args.single::<String>()?, guild_id, ctx.clone()).await {
                if let Some(author) = ctx.http.guild_member(guild_id, message.author.id).await? {
                    let author_highest_role = get_highest_role(&author, ctx.clone()).await?;
                    let target_highest_role = get_highest_role(&member, ctx.clone()).await?;
                    if target_highest_role > author_highest_role {
                        ctx.http.create_message(message.channel_id).reply(message.id)
                            .content("Cannot modify roles of someone higher on the role hierachy.")?
                            .await?;

                        return Ok(())
                    }
                    let list = args.rest().split(',').map(|s| s.trim().to_string());
                    let mut to_remove = Vec::new();
                    let mut failed = Vec::new();
                    for r1 in list {
                        if let Some((_, role)) = parse_role(r1.clone(), guild_id, ctx.clone()) {
                            to_remove.push(role);
                        } else {
                            failed.push(format!("Could not locate {}", r1));
                        }
                    }
                    let mut to_remove = filter_roles(to_remove, author_highest_role);
                    for (i, role) in to_remove.clone().iter().enumerate() {
                        if !member.roles.contains(&role.id) {
                            to_remove.remove(i);
                            failed.push(format!("You don't have {}", role.name));
                        } else if ctx.http.remove_guild_member_role(guild_id, member.user.id, role.id).await.is_err() {
                            to_remove.remove(i);
                            failed.push(format!("Failed to remove {}", role.name));
                        }
                    }
                    let mut embed = EmbedBuilder::new()
                        .title("Remove Role Summary")
                        .description(format!("Removing roles from {}", member.mention().to_string()))
                        .color(colors::RED);
                        
                    if !to_remove.is_empty() {
                        let roles = to_remove.into_iter()
                            .map(|r| r.name)
                            .collect::<Vec<String>>()
                            .join("\n");
                        let field = EmbedFieldBuilder::new("Removed Roles", roles).build();
                        
                        embed = embed.field(field);
                    }
                    if !failed.is_empty() {
                        let field = EmbedFieldBuilder::new("Failed to remove", failed.join("\n")).build();

                        embed = embed.field(field);
                    }
                    ctx.http.create_message(message.channel_id).reply(message.id)
                        .embed(embed.build()?)?
                        .await?;
                }
            }
        }
        Ok(())
    }
}