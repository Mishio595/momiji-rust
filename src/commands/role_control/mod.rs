pub mod ar;
pub mod rr;
pub mod rc;

use momiji::{
    Context,
    framework::command::ModuleBuilder,
};
use twilight_model::guild::{Member, Role};
use std::{
    collections::HashMap,
    error::Error,
    sync::Arc,
};

pub fn init_role_control(module: ModuleBuilder) -> ModuleBuilder {
    module
        .guild_only(true)
        .help_available(true)
        .add_command("ar", Arc::new(ar::AddRole))
        .add_alias("addrole", "ar")
        .add_command("rr", Arc::new(rr::RemoveRole))
        .add_alias("removerole", "rr")
        .add_command("rolecolor", Arc::new(rc::RoleColour))
        .add_alias("rc", "rolecolor")
}

fn filter_roles(roles: Vec<Role>, highest: i64) -> Vec<Role> {
    roles.into_iter()
        .filter(|role| role.position >= highest)
        .collect()
}

async fn get_highest_role(member: &Member, ctx: Context) -> Result<i64, Box<dyn Error + Send + Sync>> {
    let roles = ctx.http.roles(member.guild_id).await?;
    let roles = {
        let mut map = HashMap::new();
        for role in roles.iter() {
            map.insert(role.id, role.clone());
        }

        map
    };

    let mut pos = -1;
    for role in member.roles.iter() {
        if let Some(role) = roles.get(role) {
            if role.position > pos { pos = role.position }
        }
    }

    Ok(pos)
}