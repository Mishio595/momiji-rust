use chrono::Utc;
use momiji::{
    Context,
    core::{
        consts::{colors, BOT_INVITE, GITLAB_LINK, SUPPORT_SERV_INVITE},
        utils::{avatar_url_from_parts, seconds_to_hrtime},
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder, ImageSource};
use twilight_model::{
    channel::Message,
};
use std::{error::Error, sync::Arc};
use sysinfo::{System, SystemExt, ProcessExt};

pub struct BotInfo;
#[async_trait]
impl Command for BotInfo {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Information about the bot.")
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, _: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let guild_count = ctx.cache.stats().guilds();
        let shard_count = ctx.cluster.shards().len();
        let mut owners = String::new();
        for owner in ctx.owners.values() {
            owners = format!("{}\nName: {}#{} ({})", owners, owner.name, owner.discriminator, owner.id.0);
        }
        let sys = System::new_all();
        let system_info = format!(
            "Type: {} {}\nUptime: {}",
            sys.get_name().unwrap_or_else(|| String::from("OS Not Found")),
            sys.get_os_version().unwrap_or_else(|| String::from("Release Not Found")),
            seconds_to_hrtime(sys.get_uptime() as usize));
        let mut embed = EmbedBuilder::new()
            .description("Hi! I'm Momiji, a general purpose bot created in [Rust](http://www.rust-lang.org/) using [Twilight](https://github.com/twilight-rs/twilight/).")
            .thumbnail(ImageSource::url(avatar_url_from_parts(&ctx.user.avatar, ctx.user.id, ctx.user.discriminator.as_str()))?)
            .color(colors::MAIN)
            .field(EmbedFieldBuilder::new("Owners", owners))
            .field(EmbedFieldBuilder::new("Counts", format!("Guilds: {}\nShards: {}", guild_count, shard_count)).inline())
            .field(EmbedFieldBuilder::new("Links", format!("[Support Server]({})\n[Invite]({})\n[GitLab]({})\n", SUPPORT_SERV_INVITE, BOT_INVITE, GITLAB_LINK)).inline())
            .field(EmbedFieldBuilder::new("System Info", system_info).inline());
        if let Ok(pid) = sysinfo::get_current_pid() {
            if let Some(process) = sys.get_process(pid) {
                let process_info = format!(
                    "Memory Usage: {} MB\nCPU Usage: {}%\nUptime: {}",
                    process.memory()/1000,
                    (process.cpu_usage()*100.0).round()/100.0,
                    seconds_to_hrtime(((Utc::now().timestamp() as u64) - process.start_time()) as usize));
                embed = embed
                    .field(EmbedFieldBuilder::new("Process Info", process_info).inline());
            }
        }

        ctx.http.create_message(message.channel_id).reply(message.id)
            .embed(embed.build()?)?
            .await?;

        Ok(())
    }
}