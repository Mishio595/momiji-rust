use chrono::{Utc, Duration};
use momiji::{
    Context,
    core::{
        consts::{colors, DAY},
        utils::{
            build_welcome_embed,
            parse_user,
            parse_role,
            parse_role_alias,
            parse_welcome_items,
            user_avatar_url,
        },
    },
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder, EmbedFooterBuilder, ImageSource};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct Register;
#[async_trait]
impl Command for Register {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("A command that adds roles to a user (from the self roles list only), and depending on the settings for the command, will apply either a member role or a cooldown role with a timer. When the timer ends, cooldown is removed and member is added. In order for the switch to occur automatically, this command must be used. See the premium commands for more information on configuring this command.")
            .usage("<user_resolvable> <role_resolvables as CSV>")
            .example("@Adelyn gamer, techie")
            .permissions(Permissions::MANAGE_ROLES)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let db = ctx.db.clone();
        let http = ctx.http.clone();
        if let Some(guild_id) = message.guild_id {
            let guild_data = db.get_guild(guild_id)?;
            let roles = db.get_roles(guild_id)?;
            match parse_user(args.single::<String>().unwrap_or_default(), guild_id, ctx.clone()).await {
                Some((user_id, member)) => {
                    let channel_id = if guild_data.modlog && guild_data.modlog_channel.0 > 0 {
                        guild_data.modlog_channel
                    } else { message.channel_id };
                    let list = args.rest().split(',').map(|s| s.trim().to_string());
                    let mut to_add = Vec::new();
                    for r1 in list {
                        if let Some((r, _)) = parse_role(r1.clone(), guild_id, ctx.clone()) {
                            if guild_data.cooldown_restricted_roles.contains(&(r)) { continue; }
                            to_add.push(r);
                        } else if let Some(id) = parse_role_alias(&r1, &roles) {
                            if guild_data.cooldown_restricted_roles.contains(&(id)) { continue; }
                            to_add.push(id);
                        }
                    }
                    for (i, role_id) in to_add.clone().iter().enumerate() {
                        if member.roles.contains(role_id) {
                            to_add.remove(i);
                            continue;
                        }
                        if http.add_guild_member_role(guild_id, user_id, *role_id).await.is_err() {
                            to_add.remove(i);
                        };
                    }
                    let mut cooldown_end_time = None;
                    if let Some(role) = guild_data.register_cooldown_role {
                        http.add_guild_member_role(guild_id, user_id, role).await?;
                        if let Some(member_role) = guild_data.register_member_role {
                            let dur = match guild_data.register_cooldown_duration {
                                Some(dur) => dur,
                                None => DAY as i32,
                            };
                            let data = format!("COOLDOWN||{}||{}||{}||{}",
                                user_id.0,
                                guild_id.0,
                                member_role,
                                role);
                            let start_time = Utc::now();
                            let end_time = start_time.timestamp() + dur as i64;
                            cooldown_end_time = start_time.checked_add_signed(Duration::seconds(dur as i64));
                            db.new_timer(start_time.timestamp(), end_time, data)?;
                            ctx.tc.request();
                        }
                    } else if let Some(role) = guild_data.register_member_role {
                        http.add_guild_member_role(guild_id, user_id, role).await?;
                    }
                    let roles = if !to_add.is_empty() {
                        to_add.iter().map(|r| match ctx.cache.role(*r) {
                            Some(role) => role.name,
                            None => r.0.to_string(),
                        })
                        .collect::<Vec<String>>()
                        .join("\n")
                    } else { "No roles added".to_string() };
                    let mut embed = EmbedBuilder::new()
                        .title(format!(
                            "Registered {}#{}",
                            member.user.name,
                            member.user.discriminator,
                        ))
                        .field(EmbedFieldBuilder::new("Roles Added", roles))
                        .color(colors::MAIN)
                        .thumbnail(ImageSource::url(user_avatar_url(&member.user))?);
                    if let Some(time) = cooldown_end_time {
                        let footer = EmbedFooterBuilder::new("Cooldown ends");
                        let timestamp = time.to_rfc3339();
                        embed = embed.footer(footer).timestamp(timestamp);
                    } else {
                        embed = embed.timestamp(Utc::now().to_rfc3339())
                    }
                    http.create_message(channel_id).embed(embed.build()?)?.await?;
                    if guild_data.introduction && guild_data.introduction_channel.0 > 0 {
                        let channel = guild_data.introduction_channel;
                        if guild_data.introduction_type == "embed" {
                            let embed = build_welcome_embed(guild_data.introduction_message, &member, ctx.clone())?.build()?;
                            http.create_message(channel).embed(embed)?.await?;
                        } else {
                            http.create_message(channel).content(parse_welcome_items(guild_data.introduction_message, &member, ctx.clone()))?.await?;
                        }
                    }
                },
                None => { http.create_message(message.channel_id).reply(message.id).content("I couldn't find that user.")?.await?; }
            }
        }
        Ok(())
    }
}