use momiji::{
    Context,
    core::utils::{get_switches, parse_role},
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct EditSelfRole;
#[async_trait]
impl Command for EditSelfRole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Edit a self role. Optionally takes a category and/or aliases. This operation is lazy and won't change anything you don't specify. Replace switch tells the bot to override aliases instead of append.")
            .usage("<role_resolvable> [/c category] [/a aliases as CSV] [/replace]")
            .example("NSFW /c Opt-in /a porn, lewd /replace")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let switches = get_switches(args.full().to_string());
            let backup = String::new();
            let rest = switches.get("rest").unwrap_or(&backup);
            if let Some((role_id, d_role)) = parse_role(rest.clone(), guild_id, ctx.clone()) {
                let category = switches
                    .get("c")
                    .cloned();
                let aliases: Option<Vec<String>> = switches
                    .get("a")
                    .map(|s| s
                        .split(',')
                        .map(|c| c
                            .trim()
                            .to_string()
                            .to_lowercase())
                    .collect());
                let mut role = ctx.db.get_role(role_id, guild_id)?;
                if let Some(s) = category { role.category = s; }
                if let Some(mut a) = aliases {
                    match switches.get("replace") {
                        Some(_) => { role.aliases = a; },
                        None => { role.aliases.append(&mut a); },
                    }
                }
                let data = ctx.db.update_role(role_id, guild_id, role)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Successfully update role {} in category {} {}",
                    d_role.name,
                    data.category,
                    if !data.aliases.is_empty() {
                        format!("with aliases {}", data.aliases.join(","))
                    } else {
                        String::new()
                    }
                ))?.await?;
            } else { ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that role.")?.await?; }
        }
        
        Ok(())
    }
}