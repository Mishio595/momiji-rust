use momiji::{
    Context,
    core::consts::colors,
    core::utils::{parse_role, parse_role_alias},
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use fuzzy_match::fuzzy_match;
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder};
use twilight_http::request::AuditLogReason;
use twilight_model::{
    channel::Message,
    guild::Role,
};
use std::{error::Error, sync::Arc};

pub struct AddSelfRole;
#[async_trait]
impl Command for AddSelfRole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Add roles to yourself provided they are on the self role list.")
            .usage("<role_resolvables as CSV>")
            .example("red, green")
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let db = ctx.db.clone();
        let cache = ctx.cache.clone();
        if let Some(guild_id) = message.guild_id {
            if let Some(member) = message.member {
                let roles = db.get_roles(guild_id)?;
                let (restricted_roles, has_cooldown) = match db.get_guild(guild_id) {
                    Ok(data) => {
                        let has_cooldown = if let Some(cooldown_role) = data.register_cooldown_role {
                            member.roles.contains(&cooldown_role)
                        } else {
                            false
                        };
                        (data.cooldown_restricted_roles, has_cooldown)
                    },
                    Err(_) => {
                        (Vec::new(), false)
                    },
                };
                if !roles.is_empty() {
                    let list = args.rest().split(',').map(|s| s.trim().to_string());
                    let mut to_add = Vec::new();
                    let mut failed = Vec::new();
                    let role_names: Vec<Role> = roles.iter()
                        .filter_map(|r| cache.role(r.id))
                        .collect();
                    for r1 in list {
                        if let Some((r, r2)) = parse_role(r1.clone(), guild_id, ctx.clone()) {
                            if has_cooldown && restricted_roles.contains(&(r)) {
                                failed.push(format!("{} is not available on cooldown", r2.name));
                                continue;
                            }
                            if roles.iter().any(|e| e.id == r) {
                                to_add.push(r);
                            } else { failed.push(format!("{} is a role, but it isn't self-assignable", r2.name)); }
                        } else if let Some(id) = parse_role_alias(&r1, &roles) {
                            if has_cooldown && restricted_roles.contains(&(id)) {
                                failed.push(format!("{} is not available on cooldown", cache
                                    .guild_roles(guild_id)
                                    .and_then(|set| set.get(&id).cloned())
                                    .unwrap_or(id)
                                ));
                                continue;
                            }
                            to_add.push(id);
                        } else {
                            failed.push(format!("Failed to find match \"{}\". {}", r1,
                                if let Some(i) = fuzzy_match::<usize, Vec<(&str, usize)>>(&r1, role_names.iter().enumerate().map(|(i,r)| (r.name.as_str(), i)).collect()) {
                                    format!("Closest match: {}", role_names[i].name.clone())
                                } else { String::new() }
                            ));
                        }
                    }
                    for (i, role_id) in to_add.clone().iter().enumerate() {
                        if member.roles.contains(role_id) {
                            to_add.remove(i);
                            failed.push(format!("You already have {}", match role_names.iter().find(|r| &r.id == role_id) {
                                Some(s) => s.name.clone(),
                                None => role_id.0.to_string(),
                            }));
                        }
                        if ctx.http.add_guild_member_role(guild_id, message.author.id, *role_id).reason("Self role")?.await.is_err() {
                            to_add.remove(i);
                            failed.push(format!("Failed to add {}", match role_names.iter().find(|r| &r.id == role_id) {
                                Some(s) => s.name.clone(),
                                None => role_id.0.to_string(),
                            }));
                        };
                    }

                    
                    let mut embed = EmbedBuilder::new()
                        .title("Add Self Role Summary")
                        .color(colors::GREEN);

                    if !to_add.is_empty() {
                        let value = to_add.iter()
                            .filter_map(|r| cache.role(*r).map(|r| r.name))
                            .collect::<Vec<String>>()
                        .join("\n");

                        embed = embed.field(EmbedFieldBuilder::new("Added Roles", value));
                    }
                    if !failed.is_empty() {
                        embed = embed.field(EmbedFieldBuilder::new("Failed to Add", failed.join("\n")));
                    }

                    ctx.http.create_message(message.channel_id).reply(message.id).embed(embed.build()?)?.await?;
                } else {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("There are no self roles.")?.await?;
                }
            }
        }
        Ok(())
    }
}