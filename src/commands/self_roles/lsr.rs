use momiji::{
    Context,
    core::consts::colors,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder};
use twilight_model::channel::Message;
use std::{
    collections::BTreeMap,
    error::Error,
    sync::Arc
};

pub struct ListSelfRoles;
#[async_trait]
impl Command for ListSelfRoles {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("List all the self roles for the current server. Optionally, you can view a single category.")
            .usage("[category]")
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let db = ctx.db.clone();
        let cache = ctx.cache.clone();
        if let Some(guild_id) = message.guild_id {
            let mut roles = db.get_roles(guild_id)?;
            if !roles.is_empty() {
                if args.is_empty() {
                    let mut map: BTreeMap<String, Vec<String>> = BTreeMap::new();
                    for role in roles.iter() {
                        match cache.role(role.id) {
                            Some(r) => {
                                map.entry(role.category.clone()).or_insert_with(Vec::new).push(r.name.clone());
                            },
                            None => {
                                // Clean up roles that don't exist
                                db.del_role(role.id, guild_id)?;
                            },
                        }
                    }

                    let mut embed = EmbedBuilder::new()
                        .title("Self Roles")
                        .color(colors::MAIN);

                    for (key, val) in map.iter_mut() {
                        val.sort();
                        embed = embed.field(EmbedFieldBuilder::new(key, val.join("\n")).inline());
                    }
                    ctx.http.create_message(message.channel_id).reply(message.id).embed(embed.build()?)?.await?;
                } else {
                    let category = args.full().to_string();
                    roles.retain(|e| *e.category.to_lowercase() == category.to_lowercase());
                    if !roles.is_empty() {
                        let mut roles = roles
                            .iter()
                            .map(|e| match cache.role(e.id) {
                                Some(r) => r.name,
                                None => e.id.to_string(),
                            })
                            .collect::<Vec<String>>();
                        roles.sort();

                        let embed = EmbedBuilder::new()
                            .title(category)
                            .description(roles.join("\n"))
                            .color(colors::MAIN)
                            .build()?;

                        ctx.http.create_message(message.channel_id).reply(message.id).embed(embed)?.await?;
                    } else {
                        ctx.http.create_message(message.channel_id).reply(message.id).content(format!("The category `{}` does not exist.", category))?.await?;
                    }
                }
            } else {
                ctx.http.create_message(message.channel_id).reply(message.id).content("There are no self roles.")?.await?;
            }
        }
        Ok(())
    }
}