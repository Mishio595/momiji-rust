use momiji::{
    Context,
    core::consts::colors,
    core::utils::{parse_role, parse_role_alias},
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use fuzzy_match::fuzzy_match;
use twilight_embed_builder::{EmbedBuilder, EmbedFieldBuilder};
use twilight_http::request::AuditLogReason;
use twilight_model::{
    channel::Message,
    guild::Role,
};
use std::{error::Error, sync::Arc};

pub struct RemoveSelfRole;
#[async_trait]
impl Command for RemoveSelfRole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Remove roles from yourself provided they are on the self role list.")
            .usage("<role_resolvables as CSV>")
            .example("red, green")
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        let db = ctx.db.clone();
        let cache = ctx.cache.clone();
        if let Some(guild_id) = message.guild_id {
            if let Some(member) = message.member {
                let roles = db.get_roles(guild_id)?;
                if !roles.is_empty() {
                    let list = args.rest().split(',').map(|s| s.trim().to_string());
                    let mut to_remove = Vec::new();
                    let mut failed = Vec::new();
                    let role_names: Vec<Role> = roles.iter()
                        .filter_map(|r| cache.role(r.id))
                        .collect();
                    for r1 in list {
                        if let Some((r, r2)) = parse_role(r1.clone(), guild_id, ctx.clone()) {
                            if roles.iter().any(|e| e.id == r) {
                                to_remove.push(r);
                            } else { failed.push(format!("{} is a role, but it isn't self-assignable", r2.name)); }
                        } else if let Some(id) = parse_role_alias(&r1, &roles) {
                            to_remove.push(id);
                        } else {
                            failed.push(format!("Failed to find match \"{}\". {}", r1,
                                if let Some(i) = fuzzy_match::<usize, Vec<(&str, usize)>>(&r1, role_names.iter().enumerate().map(|(i,r)| (r.name.as_str(), i)).collect()) {
                                    format!("Closest match: {}", role_names[i].name.clone())
                                } else { String::new() }
                            ));
                        }
                    }
                    for (i, role_id) in to_remove.clone().iter().enumerate() {
                        if !member.roles.contains(role_id) {
                            to_remove.remove(i);
                            failed.push(format!("You don't have {}", match role_names.iter().find(|r| &r.id == role_id) {
                                Some(s) => s.name.clone(),
                                None => role_id.0.to_string(),
                            }));
                        }
                        if ctx.http.remove_guild_member_role(guild_id, message.author.id, *role_id).reason("Self role")?.await.is_err() {
                            to_remove.remove(i);
                            failed.push(format!("Failed to remove {}", match role_names.iter().find(|r| &r.id == role_id) {
                                Some(s) => s.name.clone(),
                                None => role_id.0.to_string(),
                            }));
                        };
                    }
                    let mut embed = EmbedBuilder::new()
                        .title("Remove Self Role Summary")
                        .color(colors::RED);

                    if !to_remove.is_empty() {
                        let value = to_remove.iter()
                            .filter_map(|r| cache.role(*r).map(|r| r.name))
                            .collect::<Vec<String>>()
                            .join("\n");

                        embed = embed.field(EmbedFieldBuilder::new("Removed Roles", value));
                    }
                    if !failed.is_empty() {
                        embed = embed.field(EmbedFieldBuilder::new("Failed to remove", failed.join("\n")));
                    }

                    ctx.http.create_message(message.channel_id).reply(message.id).embed(embed.build()?)?.await?;
                } else {
                    ctx.http.create_message(message.channel_id).reply(message.id).content("There are no self roles.")?.await?;
                }
            }
        }
        Ok(())
    }
}