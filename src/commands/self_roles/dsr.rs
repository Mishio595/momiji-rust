use momiji::{
    Context,
    core::utils::parse_role,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct DeleteSelfRole;
#[async_trait]
impl Command for DeleteSelfRole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Delete a self role.")
            .usage("<role_resolvable>")
            .example("NSFW")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            if let Some((role_id, role)) = parse_role(args.full().to_string(), guild_id, ctx.clone()) {
                ctx.db.del_role(role_id, guild_id)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Successfully deleted role {}", role.name))?.await?;
            } else { ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that role.")?.await?; }
        }
        
        Ok(())
    }
}