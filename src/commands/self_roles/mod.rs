pub mod csr;
pub mod dsr;
pub mod esr;
pub mod asr;
pub mod rsr;
pub mod lsr;

use momiji::framework::command::ModuleBuilder;
use std::sync::Arc;

pub fn init_self_role_management(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(true)
        .guild_only(true)
        .add_command("csr", Arc::new(csr::CreateSelfRole))
        .add_command("dsr", Arc::new(dsr::DeleteSelfRole))
        .add_command("esr", Arc::new(esr::EditSelfRole))
}

pub fn init_self_roles(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(true)
        .guild_only(true)
        .add_command("asr", Arc::new(asr::AddSelfRole))
        .add_alias("role", "asr")
        .add_command("rsr", Arc::new(rsr::RemoveSelfRole))
        .add_alias("derole", "rsr")
        .add_command("lsr", Arc::new(lsr::ListSelfRoles))
        .add_alias("roles", "lsr")
}