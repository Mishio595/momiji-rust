use momiji::{
    Context,
    core::utils::{get_switches, parse_role},
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct CreateSelfRole;
#[async_trait]
impl Command for CreateSelfRole {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Create a self role from a discord role. Also optionally takes a category and/or aliases.")
            .usage("<role_resolvable> [/c category] [/a aliases as CSV]")
            .example("NSFW /c Opt-in /a porn, lewd")
            .permissions(Permissions::MANAGE_GUILD)
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let switches = get_switches(args
                .full()
                .to_string());
            let backup = String::new();
            let rest = switches
                .get("rest")
                .unwrap_or(&backup);
            if let Some((role_id, role)) = parse_role(rest.clone(), guild_id, ctx.clone()) {
                let category = switches
                    .get("c")
                    .cloned();
                let aliases: Option<Vec<String>> = switches
                    .get("a")
                    .map(|s| s
                        .split(',')
                        .map(|c| c
                            .trim()
                            .to_string()
                            .to_lowercase())
                    .collect());
                let data = ctx.db.new_role(
                    role_id,
                    guild_id,
                    category,
                    aliases)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!(
                    "Successfully added role {} to category {} {}"
                    ,role.name
                    ,data.category
                    ,if !data.aliases.is_empty() {
                        format!("with aliases {}", data.aliases.join(","))
                    } else {
                        String::new()
                    }
                ))?.await?;
            } else { ctx.http.create_message(message.channel_id).reply(message.id).content("I couldn't find that role.")?.await?; }
        }
        
        Ok(())
    }
}