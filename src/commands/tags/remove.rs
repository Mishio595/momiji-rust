use momiji::{
    Context,
    core::utils::get_permissions_for_member,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct TagRemove;
#[async_trait]
impl Command for TagRemove {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Delete a tag.")
            .usage("<tag name>")
            .example("foobar")
            .guild_only(true)
            .build();

        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let tag_input = args.single_quoted::<String>()?;
            let tag = ctx.db.get_tag(guild_id, &tag_input)?;
            let check = ctx.cache.member(guild_id, message.author.id)
                .map(|m| get_permissions_for_member(m, ctx.clone()).contains(Permissions::MANAGE_MESSAGES))
                .unwrap_or(false);
            if message.author.id.0 as i64 == tag.author || check {
                let tag = ctx.db.del_tag(guild_id, &tag_input)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Successfully deleted tag `{}`", tag.name))?.await?;
            } else { ctx.http.create_message(message.channel_id).reply(message.id).content("You must own this tag in order to delete it.")?.await?; }
        }
        Ok(())
    }
}