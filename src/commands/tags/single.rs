use momiji::{
    Context,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use fuzzy_match::algorithms::{SorensenDice, SimilarityAlgorithm};
use twilight_model::channel::Message;
use std::{
    cmp::Ordering,
    error::Error,
    sync::Arc,
};

pub struct TagSingle;
#[async_trait]
impl Command for TagSingle {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("View a tag.")
            .usage("<tag name>")
            .example("foobar")
            .guild_only(true)
            .build();

        Arc::new(options)
    }

    async fn run(&self, message: Message, args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let tag_input = args.rest().trim().to_string();
            let tags = ctx.db.get_tags(guild_id)?;
            if !tags.is_empty() {
                if let Some(tag) = tags.iter().find(|e| e.name == tag_input) {
                    ctx.http.create_message(message.channel_id).reply(message.id).content(&tag.data)?.await?;
                } else {
                    let mut sdc = SorensenDice::new();
                    let mut matches = Vec::new();
                    for tag in tags.iter() {
                        let dist = sdc.get_similarity(tag.name.as_str(), &tag_input);
                        matches.push((tag, dist));
                    }
                    matches.retain(|e| e.1 > 0.2);
                    matches.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Ordering::Equal));
                    matches.truncate(5);
                    let matches = matches.iter().map(|e| e.0.name.clone()).collect::<Vec<String>>();
                    ctx.http.create_message(message.channel_id).reply(message.id).content(format!("No tag found. Did you mean...\n{}", matches.join("\n")))?.await?;
                }
            } else { ctx.http.create_message(message.channel_id).reply(message.id).content("There are no tags yet.")?.await?; }
        }
        Ok(())
    }
}