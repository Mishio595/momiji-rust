use momiji::{
    Context,
    core::utils::get_permissions_for_member,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::{
    channel::Message,
    guild::Permissions,
};
use std::{error::Error, sync::Arc};

pub struct TagEdit;
#[async_trait]
impl Command for TagEdit {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Edit a tag. Only works if you are the author.")
            .usage("<tag name, quoted> <new value>")
            .example(r#""my edited tag" I had to edit this tag"#)
            .build();

        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let tag_input = args.single_quoted::<String>()?;
            let value = args.rest().to_string();
            let mut tag = ctx.db.get_tag(guild_id, &tag_input)?;
            let check = ctx.cache.member(guild_id, message.author.id)
                .map(|m| get_permissions_for_member(m, ctx.clone()).contains(Permissions::MANAGE_MESSAGES))
                .unwrap_or(false);
            if message.author.id.0 as i64 == tag.author || check {
                tag.data = value.clone();
                let t = ctx.db.update_tag(guild_id, &tag_input, tag)?;
                ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Successfully edited tag `{}`", t.name))?.await?;
            } else { ctx.http.create_message(message.channel_id).reply(message.id).content("You must own this tag in order to edit it.")?.await?; }
        }
        Ok(())
    }
}