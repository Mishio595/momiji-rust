pub mod single;
pub mod add;
pub mod remove;
pub mod edit;
pub mod list;

use momiji::framework::command::ModuleBuilder;
use std::sync::Arc;

pub fn init_tags(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(true)
        .guild_only(true)
        .prefix("tag")
        .default_command(Arc::new(single::TagSingle))
        .add_command("show", Arc::new(single::TagSingle))
        .add_command("add", Arc::new(add::TagAdd))
        .add_command("del", Arc::new(remove::TagRemove))
        .add_command("edit", Arc::new(edit::TagEdit))
        .add_command("list", Arc::new(list::TagList))
}