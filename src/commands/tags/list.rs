use momiji::{
    Context,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::channel::Message;
use std::{error::Error, sync::Arc};

pub struct TagList;
#[async_trait]
impl Command for TagList {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Alias to `tag list`")
            .guild_only(true)
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, _: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let tags = ctx.db.get_tags(guild_id)?;
            if !tags.is_empty() {
                ctx.http.create_message(message.channel_id).reply(message.id).content(tags.iter().map(|e| e.name.as_str()).collect::<Vec<&str>>().join("\n"))?.await?;
            } else {
                ctx.http.create_message(message.channel_id).reply(message.id).content("No tags founds.")?.await?;
            }
        }
        Ok(())
    }
}