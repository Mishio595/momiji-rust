use momiji::{
    Context,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_model::channel::Message;
use std::{error::Error, sync::Arc};

pub struct TagAdd;
#[async_trait]
impl Command for TagAdd {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Create a new tag.")
            .usage("<tag name, quoted> <tag value>")
            .example(r#""my new tag" look, I made a tag!"#)
            .guild_only(true)
            .build();
            
        Arc::new(options)
    }

    async fn run(&self, message: Message, mut args: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(guild_id) = message.guild_id {
            let tag_input = args.single_quoted::<String>()?;
            let value = args.rest().to_string();
            let tag = ctx.db.new_tag(message.author.id, guild_id, tag_input.clone(), value)?;
            ctx.http.create_message(message.channel_id).reply(message.id).content(format!("Successfully created tag `{}`", tag.name))?.await?;
        }
        Ok(())
    }
}