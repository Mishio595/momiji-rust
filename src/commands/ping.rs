use momiji::{
    Context,
    core::consts::colors,
    framework::{
        args::Args,
        command::{Command, Options}
    },
};
use twilight_embed_builder::EmbedBuilder;
use twilight_model::channel::Message;
use std::{error::Error, sync::Arc};

pub struct Ping;
#[async_trait]
impl Command for Ping {
    fn options(&self) -> Arc<Options> {
        let options = Options::builder()
            .description("Make sure the bot is alive.")
            .build();
        
        Arc::new(options)
    }

    async fn run(&self, message: Message, _: Args, ctx: Context) -> Result<(), Box<dyn Error + Send + Sync>> {
        use chrono::DateTime;
        let shard_latency = {
            let info = ctx.cluster.info();
            // This only works in a single sharded situation
            info.get(&0).and_then(|info| info.latency().average())
                .map(|dur| dur.as_millis().to_string())
                .unwrap_or_else(|| "unknown".to_string())
        };
        let embed = EmbedBuilder::new()
            .title("Pong!")
            .color(colors::MAIN)
            .description(format!("**Shard Latency:** {}", shard_latency));
        let response = ctx.http.create_message(message.channel_id).reply(message.id)
            .embed(embed.clone().build()?)?
            .await?;
        let rtt =
            DateTime::parse_from_rfc3339(response.timestamp.as_str()).map(|ts| ts.timestamp_millis()).unwrap_or(0)
            - DateTime::parse_from_rfc3339(message.timestamp.as_str()).map(|ts| ts.timestamp_millis()).unwrap_or(0);
        let embed = embed
            .description(format!("**Shard Latency:** {} ms\n**HTTP Response Time:** {} ms", shard_latency, rtt));
        ctx.http.update_message(response.channel_id, response.id).embed(embed.build()?)?.await?;
        
        Ok(())
    }
}