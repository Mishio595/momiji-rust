pub mod owner_only;
pub mod config;
pub mod prune;
pub mod self_roles;
pub mod tags;
pub mod botinfo;
pub mod ping;
pub mod prefix;
pub mod reminder;
pub mod role_control;
pub mod register;

pub use config::init_config;
pub use owner_only::{
    init_owner_commands,
    init_db_controls,
};
pub use tags::init_tags;
pub use self_roles::{
    init_self_role_management,
    init_self_roles,
};
pub use role_control::init_role_control;

use momiji::framework::command::ModuleBuilder;
use std::sync::Arc;

pub fn init_management(module: ModuleBuilder) -> ModuleBuilder {
    module
        .guild_only(true)
        .help_available(true)
        // .add_command("setup", Arc::new(SetupMute))
        .add_command("prune", Arc::new(prune::Prune))
        .add_alias("purge", "prune")
        // .add_command("cleanup", Arc::new(Cleanup))
}

pub fn init_misc(module: ModuleBuilder) -> ModuleBuilder {
    module
        .help_available(true)
        .add_command("register", Arc::new(register::Register))
        .add_alias("reg", "register")
        // .add_command("anime", Arc::new(Anime))
        .add_command("botinfo", Arc::new(botinfo::BotInfo))
        .add_alias("bi", "botinfo")
        // .add_command("cat", Arc::new(Cat))
        // .add_command("dog", Arc::new(Dog))
        // .add_command("joke", Arc::new(DadJoke))
        // .add_command("manga", Arc::new(Manga))
        // .add_command("now", Arc::new(Now))
        .add_command("ping", Arc::new(ping::Ping))
        .add_command("prefix", Arc::new(prefix::Prefix))
        .add_command("remind", Arc::new(reminder::Reminder))
        // .add_command("roleinfo", Arc::new(RoleInfo))
        // .add_command("roll", Arc::new(Roll))
        // .add_command("serverinfo", Arc::new(ServerInfo))
        .add_command("tags", Arc::new(tags::list::TagList))
        // .add_command("urban", Arc::new(Urban))
        // .add_command("uid", Arc::new(UserId))
        // .add_command("userinfo", Arc::new(UserInfo))
        // .add_command("weather", Arc::new(Weather))
        // .add_command("stats", Arc::new(Stats))
}

// pub fn init_ignore(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .guild_only(true)
//         .help_available(true)
//         .prefix("ignore")
//         .default_command(IgnoreList)
//         .add_command("add", Arc::new(IgnoreAdd))
//         .add_command("remove", Arc::new(IgnoreRemove))
//         .add_command("list", Arc::new(IgnoreList))
//         .add_command("level", Arc::new(IgnoreLevel))
// }

// pub fn init_tests(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .guild_only(true)
//         .help_available(true)
//         .prefix("test")
//         .add_command("welcome", Arc::new(TestWelcome))
//         .add_command("intro", Arc::new(TestIntro))
// }

// pub fn init_nsfw() -> CreateGroup {
//     CreateGroup::default()
//         .help_available(true)
//         .check(|_,message,_,_| {
//             if let Ok(channel) = message.channel_id.to_channel() {
//                 if channel.is_nsfw() {
//                     true
//                 } else {
//                     check_error!(message.channel_id.say("Command only available in NSFW channels."));
//                     false
//                 }
//             } else {
//                 check_error!(message.channel_id.say("Failed to get the channel info. I can't tell if this channel is NSFW."));
//                 false
//         }})
//         .cmd("e621", Furry)
// }

// pub fn init_info(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .guild_only(true)
//         .help_available(true)
//         .add_command("modinfo", Arc::new(ModInfo))
// }

// pub fn init_kickbans(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .guild_only(true)
//         .help_available(true)
//         .add_command("ban", Arc::new(BanUser))
//         .add_command("kick", Arc::new(KickUser))
// }

// pub fn init_mute(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .guild_only(true)
//         .help_available(true)
//         .add_command("mute", Arc::new(Mute))
//         .add_command("unmute", Arc::new(Unmute))
// }

// pub fn init_notes(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .prefix("note")
//         .guild_only(true)
//         .help_available(true)
//         .add_command("add", Arc::new(NoteAdd))
//         .add_command("del", Arc::new(NoteRemove))
//         .add_command("list", Arc::new(NoteList))
// }

// pub fn init_watchlist(module: ModuleBuilder) -> ModuleBuilder {
//     module
//         .prefixes(vec!["watchlist", "wl"])
//         .guild_only(true)
//         .help_available(true)
//         .default_cmd(WatchlistList)
//         .add_command("add", Arc::new(WatchlistAdd))
//         .add_command("del", Arc::new(WatchlistRemove))
//         .add_command("list", Arc::new(WatchlistList))
// }